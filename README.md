# 楽曲検索システム（Song Search System）

## 概要
* 使用者が曲名、歌手名、歌詞、ジャンル、番組名をキーとして楽曲の検索が可能なシステム
* 管理者はシステムに対して楽曲情報を登録・変更することが可能であり、検索者の情報も閲覧可能である
* APサーバとしてApache Tomcat上で動作するJava 8のサーブレット・JavaBeans・JSPを用いる
* オンラインでないとサービスは利用できないが、Webアプリ自体はオフライン対応PWAである

## ネットワーク図
<a href="../raw/master/ネットワーク図.png"><img src="song-search-system/raw/master/ネットワーク図.png" width="95%" /></a>

## 開発者へ
* 開発者向けドキュメントは[Wiki](../../wikis/home)に移動しました
