<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<!DOCTYPE html>
<html lang="ja">
<jsp:include page="/include/head.jsp" ><jsp:param name="title" value="会員情報変更" /></jsp:include>
<body>
	<%@ include file="/include/headBox.jsp" %>
	<%
		try {
			if ((boolean) session.getAttribute("login")) {
	%>
	<%@ include file="/include/nav.jsp" %>
	<%
				if (session.getAttribute("caution") != null && (boolean) session.getAttribute("caution")) {
					out.println("<p style=\"color: red\">入力に誤りがあります</p>");
					out.println("<p style=\"color: red\">指示通りに入力してください。</p>");
				}
	%>

	<div class="container">
			<h3><span id="under">会員情報変更</span></h3>
		<p class="text-center">パスワードは再度入力する必要があります</p>
		<div class="row">
			<div class="col">
				<form action="UserController" method="post">
					<dl>
						<div class="form-group row">
							<dt class="col-sm-2">ユーザID</dt>
							<dd class="col-sm-5"><%=session.getAttribute("oldId")%></dd>
							<dd class="col-sm-5">
								<input type="text" name="newId" class="form-control" minlength="5" maxlength="10" value="<%=(session.getAttribute("newId") != null) ?
									session.getAttribute("newId") : session.getAttribute("oldId")%>" required />
							</dd>
						</div>
						<div class="form-group row">
							<dt class="col-sm-2">パスワード</dt>
							<dd class="col-sm-5">（プライバシのため表示されません）</dd>
							<dd class="col-sm-5">
								<input type="password" name="newPassword" class="form-control" minlength="8" maxlength="16" value="<%=(session.getAttribute("newPassword") != null) ?
									session.getAttribute("newPassword") : session.getAttribute("oldPassword")%>" required />
							</dd>
						</div>
						<div class="form-group row">
							<dt class="col-sm-2">ユーザネーム</dt>
							<dd class="col-sm-5"><%=session.getAttribute("oldUserName")%></dd>
							<dd class="col-sm-5">
								<input type="text" name="newName" class="form-control" minlength="2" maxlength="20" value="<%=(session.getAttribute("newId") != null) ?
									session.getAttribute("newName") : session.getAttribute("oldUserName")%>" required />
							</dd>
						</div>
						<div class="form-group row">
							<dt class="col-sm-2">年齢</dt>
							<dd class="col-sm-5"><%=session.getAttribute("oldAge")%></dd>
							<dd class="col-sm-5">
								<input type="number" name="newAge" class="form-control" value="<%=(session.getAttribute("newId") != null) ?
									session.getAttribute("newAge") : session.getAttribute("oldAge")%>" required />
							</dd>
						</div>
						<div class="form-group row">
							<dt class="col-sm-2">性別</dt>
							<dd class="col-sm-5"><%=session.getAttribute("oldSex")%></dd>
							<dd class="col-sm-5">
								<label>
								<input type="radio" name="newSex" value="man" required
								<%=(session.getAttribute("newSex") != null && session.getAttribute("newSex").equals("男性")) ||
									(session.getAttribute("newSex") == null && session.getAttribute("oldSex").equals("男性")) ?
										"checked=\"checked\"" : ""%> />男性</label>
									<label>
								<input type="radio" name="newSex" value="woman"
								<%=(session.getAttribute("newSex") != null && session.getAttribute("newSex").equals("女性")) ||
									(session.getAttribute("newSex") == null && session.getAttribute("oldSex").equals("女性")) ?
										"checked=\"checked\"" : ""%> />女性</label>
							</dd>
						</div>
					</dl>
					<div class="row justify-content-center">
						<button id="create"type="submit" name="action" value="checkSend2" class="btn btn-primary">変更する</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<%
		} else {
				out.println("<h2>不正なセッション</h2>");
				out.println("<p>セッションが切れた、もしくは不正であるためログインしなおしてください。</p>");
				out.println("<a href=\"./index.jsp\">ログインページ</a>");
			}
		} catch (NullPointerException e) {
			out.println("<h2>不正なセッション</h2>");
			out.println("<p>ログインをしてください。</p>");
			out.println("<a href=\"./index.jsp\">ログインページ</a>");
		}
	%>
   <style>
  h3{
    text-align: center;
  }
    #ch-box{
      margin-right: 20px;
    }
  </style>

	<%@ include file="/include/foot.jsp" %>
</body>
</html>