<%@ page import="model.Song"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<!DOCTYPE html>
<html lang="ja">
<jsp:include page="/include/head.jsp" ><jsp:param name="title" value="楽曲変更画面" /></jsp:include>
<body>
	<%@ include file="/include/headBox.jsp" %>
	<%
		try {
			if ((boolean) session.getAttribute("login")) {
				Song nSong = null;
				if(session.getAttribute("changeSong") != null){
					nSong = (Song)session.getAttribute("changeSong");
				}
	%>
	<%@ include file="/include/nav.jsp" %>

<form action="SearchController" method="post">
	<div class="container-fluid w-75">
		<h3><span id="under">楽曲情報変更</span></h3>
		<p>歌手名、作曲者、作詞者は ';'または','で複数人入力できます。<br>スペースで入力しないでください。 *は必須</p>

	<%
		if (session.getAttribute("caution") != null && (boolean) session.getAttribute("caution")) {
					out.println("<p style=\"color: red\">入力に空欄や不正があります。</p>");
					session.setAttribute("caution", false);
				}
	%>
		<dl class="row">
			<dt class="col-sm-2">*曲名</dt>
			<dd class="col-sm-5"><%=session.getAttribute("oldTitle") %></dd>
			<dd class="col-sm-5">
				<input type="text" name="newTitle" class="form-control"
					value="<%= (nSong != null && nSong.getSongName() != null) ?
						nSong.getSongName() : session.getAttribute("oldTitle") %>" required />
			</dd>
			<dt class="col-sm-2">*歌手名</dt>
			<dd class="col-sm-5"><%=session.getAttribute("oldSinger") %></dd>
			<dd class="col-sm-5">
				<input type="text" name="newSinger" class="form-control"
					value="<%= (nSong != null && nSong.getSinger() != null) ?
						nSong.getSinger() : session.getAttribute("oldSinger") %>" required />
			</dd>
			<dt class="col-sm-2">作曲者</dt>
			<dd class="col-sm-5"><%=session.getAttribute("oldComposer") %></dd>
			<dd class="col-sm-5">
				<input type="text" name="newComposer" class="form-control"
					value="<%= (nSong != null && nSong.getComposer() != null) ?
						nSong.getComposer() : session.getAttribute("oldComposer") %>" />
			</dd>
			<dt class="col-sm-2">作詞者</dt>
			<dd class="col-sm-5"><%=session.getAttribute("oldLyricist") %></dd>
			<dd class="col-sm-5">
				<input type="text" name="newLyricist" class="form-control"
					value="<%= (nSong != null && nSong.getLyricist() != null) ?
						nSong.getLyricist() : session.getAttribute("oldLyricist") %>" />
			</dd>
			<dt class="col-sm-2">番組名</dt>
			<dd class="col-sm-5"><%=session.getAttribute("oldProgram") %></dd>
			<dd class="col-sm-5">
				<input type="text" name="newProgram" class="form-control"
					value="<%= (nSong != null && nSong.getProgramName() != null) ?
						nSong.getProgramName() : (
							(session.getAttribute("oldProgram").equals("none")) ?
								"" : session.getAttribute("oldProgram")
						) %>" />
			</dd>
			<dt class="col-sm-2">*ジャンル</dt>
			<dd class="col-sm-5"><%=session.getAttribute("oldGenre") %></dd>
			<dd class="col-sm-5">
				<select name="newGenre" class="form-control" required>
					<option value="overall">総合</option>
					<option value="j-pop">J-POP</option>
					<option value="westan">洋楽</option>
					<option value="jazz">ジャズ</option>
					<option value="enka">演歌</option>
					<option value="k-pop">K-POP</option>
					<option value="japanese-rock">邦楽ロック</option>
					<option value="nursery-rhymes">童謡</option>
					<option value="anime">アニメ</option>
					<option value="vocaloid">ボーカロイド</option>
					<option value="game">ゲーム</option>
				</select>
			</dd>
			<dt class="col-sm-2">*歌詞</dt>
			<pre class="col-sm-10 mt-2"><%=session.getAttribute("oldLyrics")%></pre>
			<dd class="offset-md-2 col-sm-10">
				<textarea cols="50" rows="10" name="newLyrics" class="form-control"
					required><%= (nSong != null && nSong.getLyrics() != null) ?
						nSong.getLyrics() : session.getAttribute("oldLyrics")%></textarea>
			</dd>
		</dl>
	</div>

	<div class="row justify-content-center">
		<button type="submit" name="action" value="checkSend2" class="btn btn-success">変更確認</button>
	</div>
</form>
	<%
		} else {
				out.println("<h2>不正なセッション</h2>");
				out.println("<p>セッションが切れた、もしくは不正であるためログインしなおしてください。</p>");
				out.println("<a href=\"./index.jsp\">ログインページ</a>");
			}
		} catch (NullPointerException e) {
			out.println("<h2>不正なセッション</h2>");
			out.println("<p>ログインをしてください。</p>");
			out.println("<a href=\"./index.jsp\">ログインページ</a>");
		}
	%>
	<%@ include file="/include/foot.jsp" %>
</body>
</html>