<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<!DOCTYPE html>
<html lang="ja">
<jsp:include page="/include/head.jsp"><jsp:param name="title" value="確認画面" /></jsp:include>
<body>
	<%@ include file="/include/headBox.jsp" %>
	<%@ include file="/include/nav.jsp" %>

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-sm-8">
				<form action="UserController" method="post">
					<h3>
						<span id="userID"><%=session.getAttribute("newId")%></span>の会員情報
					</h3>
					<dl class="row">
						<dt class="col-sm-5">ユーザID</dt>
						<dd class="col-sm-7"><%=session.getAttribute("newId")%></dd>
						<dt class="col-sm-5">パスワード</dt>
						<dd class="col-sm-7">(プライバシのため表示されません)</dd>
						<dt class="col-sm-5">ユーザネーム</dt>
						<dd class="col-sm-7"><%=session.getAttribute("newName")%></dd>
						<dt class="col-sm-5">年齢</dt>
						<dd class="col-sm-7"><%=session.getAttribute("newAge")%></dd>
						<dt class="col-sm-5">性別</dt>
						<dd class="col-sm-7"><%=session.getAttribute("newSex")%></dd>
					</dl>
					<div class="row justify-content-center">
						<button type="submit" name="action" value="change" class="btn btn-primary">確定</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<style>
		dt, dd {
			margin-top: 10px;
			margin-bottom: 10px;
		}
	</style>

	<%@ include file="/include/foot.jsp" %>
</body>
</html>