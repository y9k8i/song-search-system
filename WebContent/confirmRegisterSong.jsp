<%@ page import="model.Song"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<!DOCTYPE html>
<html lang="ja">
<jsp:include page="/include/head.jsp" ><jsp:param name="title" value="楽曲情報登録確認" /></jsp:include>
<body>
	<header>
	<%@ include file="/include/headBox.jsp" %>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<button type="button" class="btn btn-warning" onclick="history.back()">戻る</button>
	</nav>
	</header>
	<%
		try {
			if ((boolean) session.getAttribute("login")) {
				Song detail = (Song) session.getAttribute("registSong");
	%>

	<div class="container">
		<div class="row justify-content-center">
			<h3><span id="under">楽曲情報登録確認</span></h3>
			<dl class="row" id="result">
				<dt class="col-sm-2">歌手名</dt>
				<dd class="col-sm-4"><%=detail.getSinger()%></dd>
				<dt class="col-sm-2">曲名</dt>
				<dd class="col-sm-4"><%=detail.getSongName()%></dd>
				<dt class="col-sm-2">作詞名</dt>
				<dd class="col-sm-4"><%=detail.getLyricist()%></dd>
				<dt class="col-sm-2">作曲名</dt>
				<dd class="col-sm-4"><%=detail.getComposer()%></dd>
				<dt class="col-sm-2">番組名</dt>
				<dd class="col-sm-4"><%=
					detail.getProgramName().equals("none") ? "なし" : detail.getProgramName() %>
				</dd>
				<dt class="col-sm-2">ジャンル</dt>
				<dd class="col-sm-4"><%=detail.getGenreName()%></dd>
				<dt class="col-sm-2">歌詞</dt>
				<pre class="col-sm-10 mt-2"><%=detail.getLyrics()%></pre>
			</dl>
			<form action="SearchController" method="post">
				<div class="row justify-content-center">
					<button type="submit" name="action" value="regist" class="btn btn-danger">確定</button>
				</div>
			</form>
		</div>
	</div>
	<%
		} else {
				out.println("<h2>不正なセッション</h2>");
				out.println("<p>セッションが切れた、もしくは不正であるためログインしなおしてください。</p>");
				out.println("<a href=\"./index.jsp\">ログインページ</a>");
			}
		} catch (NullPointerException e) {
			out.println("<h2>不正なセッション</h2>");
			out.println("<p>ログインをしてください。</p>");
			out.println("<a href=\"./index.jsp\">ログインページ</a>");
		}
	%>

	<%@ include file="/include/foot.jsp" %>
</body>
</html>