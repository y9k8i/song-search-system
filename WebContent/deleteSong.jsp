<%@page import="model.Song"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<!DOCTYPE html>
<html lang="ja">
<jsp:include page="/include/head.jsp" ><jsp:param name="title" value="楽曲情報削除確認" /></jsp:include>
<body>
	<%@ include file="/include/headBox.jsp" %>
	<%
		try {
			if ((boolean) session.getAttribute("login") ) {
				Song detailDel = (Song) session.getAttribute("detailSong");
	%>
	<%@ include file="/include/nav.jsp" %>

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-sm-8">
				<h3>楽曲情報削除確認</h3>
				<dl class="row">
					<dt class="col-sm-5">歌手名</dt>
					<dd class="col-sm-7"><%=detailDel.getSinger()%></dd>
					<dt class="col-sm-5">曲名</dt>
					<dd class="col-sm-7"><%=detailDel.getSongName()%></dd>
					<dt class="col-sm-5">作詞名</dt>
					<dd class="col-sm-7"><%=detailDel.getLyricist()%></dd>
					<dt class="col-sm-5">作曲名</dt>
					<dd class="col-sm-7"><%=detailDel.getComposer()%></dd>
					<dt class="col-sm-5">番組名</dt>
					<dd class="col-sm-7">
						<%=detailDel.getProgramName().equals("none") ? detailDel.getProgramName() : "なし"%>
					</dd>
					<dt class="col-sm-5">ジャンル</dt>
					<dd class="col-sm-7"><%=detailDel.getGenreName()%></dd>
				</dl>
				<br> 歌詞
				<br>
				<textarea readonly cols="50" rows="10" name="lyrics"><%=detailDel.getLyrics()%></textarea>
				<br>
				<br>
				<form action="SearchController" method="post">
					<div class="row justify-content-center">
						<button type="submit" name="action" value="delete" class="btn btn-danger">削除</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<%
		} else {
				out.println("<h2>不正なセッション</h2>");
				out.println("<p>セッションが切れた、もしくは不正であるためログインしなおしてください。</p>");
				out.println("<a href=\"./index.jsp\">ログインページ</a>");
			}
		} catch (NullPointerException e) {
			out.println("<h2>不正なセッション</h2>");
			out.println("<p>ログインをしてください。</p>");
			out.println("<a href=\"./index.jsp\">ログインページ</a>");
		}
	%>

	<%@ include file="/include/foot.jsp" %>
</body>
</html>