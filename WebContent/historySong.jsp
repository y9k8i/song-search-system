<%@page import="model.SearchHistory"%>
<%@page import="model.Song"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>

<!DOCTYPE html>
<html lang="ja">
<jsp:include page="/include/head.jsp" ><jsp:param name="title" value="履歴" /></jsp:include>
<body>
	<%@ include file="/include/headBox.jsp" %>
	<%
		try {
			if ((boolean) session.getAttribute("login")) {
	%>
	<%@ include file="/include/nav.jsp" %>
	<%
		if ((boolean) session.getAttribute("history")) {
	%>
	<div class="container">
		<h3 class="text-center"><span id="under">履歴</span></h3>
		<div class="table-responsive">
		<div style="height:300px; overflow-y:scroll;">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>歌手名</th>
						<th>曲名</th>
						<th>日付(年月日)</th>
					</tr>
				</thead>
				<tbody>
					<%
						Song[] song = (Song[]) session.getAttribute("histSongInfo");
						SearchHistory[] hist = (SearchHistory[]) session.getAttribute("histData");
						for (int i = 0; i < hist.length; i++) {
							out.print("<tr>");
							out.print("<td>" + (i + 1) + "</td>");
							out.print("<td><form action=\"SearchController\" method=\"post\" name=\"sForm" + i
									+ "\"><input type=\"hidden\" name=\"singer\" value=\""
									+ song[i].getSinger() + "\">"
									+ "<input type=\"hidden\"name=\"action\" value=\"search\">"
									+ "<a href=\"javascript:sForm" + i + ".submit()\">"
									+ song[i].getSinger()
									+ "</a></form></td>");
							out.print(
									"<td><form action=\"SearchHistoryController\" method=\"post\" name=\"form" + i
											+ "\"><input type=\"hidden\" name=\"songid\" value=\""
											+ song[i].getSongID() + "\"><a href=\"javascript:form" + i + ".submit()\">"
											+ song[i].getSongName()
											+ "</a></form></td>");
							out.print("<td>" + hist[i].getSearchDate() + "</td>");
							out.print("</tr>");
						}
					%>
				</tbody>
			</table>
		</div>
		</div>
	</div>
	<%
			session.setAttribute("history", false);
		} else {
	%>
	<div class="container">
		<h3 class="text-center"><span id="under">履歴</span></h3>
		<p class="text-center">履歴はまだありません</p>
	</div>
	<%
		}
			} else {
				out.println("<h2>不正なセッション</h2>");
				out.println("<p>セッションが切れた、もしくは不正であるためログインしなおしてください。</p>");
				out.println("<a href=\"./index.jsp\">ログインページ</a>");
			}
		} catch (NullPointerException e) {
			out.println("<h2>不正なセッション</h2>");
			out.println("<p>ログインをしてください。</p>");
			out.println("<a href=\"./index.jsp\">ログインページ</a>");
		}
	%>

	<%@ include file="/include/foot.jsp" %>
</body>
</html>