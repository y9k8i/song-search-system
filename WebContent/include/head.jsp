<%@ page language="java" pageEncoding="UTF-8"%><head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title><%=request.getParameter("title")%></title>
<!-- CSS読み込み -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css" type="text/css">
<%-- テーマカラー --%>
<meta name="theme-color" content="#77e8ca">
<meta name="msapplication-TileColor" content="#77e8ca">
</head>