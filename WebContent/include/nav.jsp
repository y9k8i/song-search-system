<%@ page language="java" pageEncoding="UTF-8"%><nav class="navbar navbar-expand-lg navbar-light sticky-top bg-light mb-5">
	<button type="button" class="btn btn-warning my-sm-2" onclick="history.back()">戻る</button>
	<a class="navbar-brand m-auto d-lg-none" href="top.jsp">
		<img src="apple-touch-icon.png" width="42" height="42">
	</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
		aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
		<div class="navbar-nav mr-auto"></div>
		<div class="navbar-nav">
			<a href="./top.jsp">
				<button type="button" class="btn btn-outline-success form-inline my-0 my-sm-2 col-12 col-lg-auto">トップ</button>
			</a>
			<form action="SearchHistoryController" method="post" class="form-inline">
				<button type="submit" name="action" value="history"
					class="btn btn-outline-success my-0 my-sm-2 col-12 col-lg-auto">履歴</button>
			</form>
			<% if (session.getAttribute("admin") != null && (boolean) session.getAttribute("admin")) { %>
			<form action="SearchController" method="post" class="form-inline">
				<button type="submit" name="action" value="registerSong"
					class="btn btn-outline-success my-0 my-sm-2 col-12 col-lg-auto">楽曲情報登録</button>
			</form>
			<% } %>
			<form action="UserController" method="post" class="form-inline my-0">
				<% if (session.getAttribute("admin") != null && (boolean) session.getAttribute("admin")) { %>
				<button type="submit" name="action" value="listMember"
					class="btn btn-outline-success my-0 my-sm-2 col-12 col-lg-auto">会員一覧</button>
				<% } else { %>
				<button type="submit" name="action" value="changeMemberInfo"
					class="btn btn-outline-success my-0 my-sm-2 col-12 col-lg-auto">会員情報変更</button>
				<% } %>
				<button type="submit" name="action" value="logout"
					class="btn btn-outline-success my-0 my-sm-2 col-12 col-lg-auto">ログアウト</button>
			</form>
		</div>
	</div>
	</nav>