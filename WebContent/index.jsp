<%@ page import="java.io.File"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<%
// 本番環境などDocker内部におけるHTTPでのアクセスはHTTPSにリダイレクトする
if(request.getScheme() == "http" && new File("/.dockerenv").exists()) {
	response.setStatus(301);
	response.setHeader( "Location", "https://" + request.getServerName() + "/" );
	response.setHeader( "Connection", "close" );
}
%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>ログイン</title>
<%-- CSS読み込み --%>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css" type="text/css">
<%--ファビコン --%>
<link rel="shortcut icon" href="favicon.ico">
<link rel="icon" type="image/x-icon" href="favicon.ico">
<%-- PWA対応 --%>
<meta name="application-name" content="楽曲検索システム">
<meta name="theme-color" content="#77e8ca">
<meta name="msapplication-square310x310logo" content="/site-tile-310x310.png">
<meta name="msapplication-TileColor" content="#77e8ca">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-title" content="SSS">
<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="512x512" href="android-chrome-512x512.png">
<link rel="manifest" href="manifest.json">
<%-- ServiceWorkerの登録 --%>
<script>
if ('serviceWorker' in navigator) {
	window.addEventListener('load', function() {
		navigator.serviceWorker.register('service-worker.js').then(reg => {
			reg.update();
		}).catch(console.error.bind(console));
		// キャッシュ期限は60秒
		if (Date.now() - <%= System.currentTimeMillis() %> > 60 * 1000) {
			var str = '<div class="my-5 col text-center text-danger '
				+ 'text-nowrap align-items-center"><h2>キャッシュは<wbr>期限切れです';
			if (navigator.standalone)
				str += '<wbr>アプリを削除<wbr>してください';
			str += '</h2></div>';
			$(".row")[0].innerHTML= str;
			localStorage.clear();
			navigator.serviceWorker.getRegistrations()
			.then(registrations => {
				for (let registration of registrations) {
					registration.unregister();
				}
			});
			caches.delete('SSS-cache');
		}
	});
}
</script>
<%-- 戻るボタン非表示&イベント無効化 --%>
<style>
	.btn-warning {
		opacity: 0;
		pointer-events: none;
	}
</style>
<style>
	.login-top-image {
		display: inline-block;
		font: 'メイリオ';
		background: #77e8ca;
		letter-spacing: 5px;
		color: #fff;
		font-size: 56px;
		font-weight: bold;
		user-select: none
	}

	.top-box {
		background: #77e8ca;
	}
</style>
</head>
<body>
	<div class="top-box d-flex">
		<span class="login-top-image m-auto"> <span id="song-S">S</span>ONG<br>
		<span id="search-S">S</span>EARCH<br>
		<span id="sistem-S">S</span>YSTEM
		</span>
	</div>
	<div id="alert-offline" class="alert alert-warning fade fixed-top" role="alert">
		<strong>オフラインです!</strong> オンラインになるまでシステムは利用できません
	</div>
	<div id="alert-online" class="alert alert-info fade fixed-top" role="alert">
		オンラインになりました
	</div>
<%
if(session.getAttribute("login") != null && !(Boolean)session.getAttribute("login")) {
	out.println("<p style=\"color: red\">ユーザ名またはパスワードが違います</p>");
} else if(session.getAttribute("admin") != null){
	session.invalidate();
}
%>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-lg-7">
				<h3 style="text-align: center">LOGIN</h3>
				<form action="UserController" method="post">
					<div class="form-group row">
						<label for="text3a" class="col-sm-3 col-form-label">ユーザID</label>
						<div class="col-sm-9">
							<input type="text" name="id" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<label for="text3b" class="col-sm-3 col-form-label">パスワード</label>
						<div class="col-sm-9">
							<input type="password" name="password" class="form-control">
						</div>
					</div>
					<div class="form-group row justify-content-center">
						<button type="submit" name="action" value="login"
							class="btn btn-primary">LOGIN</button>
					</div>
				</form>
			</div>
			<div class="col-md-4 col-lg-5 d-flex justify-content-center align-items-center">
				<div>
					<div class="row justify-content-center">
						<a href="./registerMember.jsp">
							<button type="submit" class="btn btn-primary">新規登録</button>
						</a>
					</div>
					<div class="row justify-content-center">
						<br>
						<p class="text-center">はじめての方はこちらから登録してください</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%@ include file="/include/foot.jsp" %>
	<%-- オフライン関連アラート制御 --%>
	<script>
	$(function () {
		if (navigator.onLine == false) {
			onOffline();
		}
	});
	window.addEventListener("online", function() {
		onOnline();
	});
	window.addEventListener("offline", function() {
		onOffline();
	});
	function onOnline() {
		$(".form-control").prop('disabled', false);
		$(".btn").prop('disabled', false);
		$("#alert-offline").removeClass("show");
		$("#alert-online").addClass("show");
		setTimeout(function(){
			$("#alert-online").removeClass("show");
		}, 3000);
	}
	function onOffline() {
		$(".form-control").prop('disabled', true);
		$(".btn").prop('disabled', true);
		$("#alert-online").removeClass("show");
		$("#alert-offline").addClass("show");
		setTimeout(function(){
			$("#alert-offline").removeClass("show");
		}, 3000);
	}
	</script>
</body>
</html>