<%@page import="model.Member"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<!DOCTYPE html>
<html lang="ja">
<jsp:include page="/include/head.jsp" ><jsp:param name="title" value="会員情報" /></jsp:include>
<body>
	<%@ include file="/include/headBox.jsp" %>
	<%@ include file="/include/nav.jsp" %>
	<%
		try {
			if ((boolean) session.getAttribute("login") && (boolean) session.getAttribute("admin")) {
				Member detail = (Member) session.getAttribute("detailMember");
	%>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-sm-6">
				<h3>
					<span id="userID">
						<%=detail.getUserID()%>
					</span>の会員情報
				</h3>
				<dl class="row">
					<dt class="col-sm-5">ユーザID</dt>
					<dd class="col-sm-7">
						<%=detail.getUserID()%>
					</dd>
					<dt class="col-sm-5">パスワード</dt>
					<dd class="col-sm-7">(プライバシのため表示されません)</dd>
					<dt class="col-sm-5">ユーザネーム</dt>
					<dd class="col-sm-7">
						<%=detail.getUserName()%>
					</dd>
					<dt class="col-sm-5">年齢</dt>
					<dd class="col-sm-7">
						<%=detail.getAge()%>
					</dd>
					<dt class="col-sm-5">性別</dt>
					<dd class="col-sm-7">
						<%=detail.getSex()%>
					</dd>
				</dl>
				<div class="row justify-content-center">
					<form action="UserController" method="post">
						<button type="submit" name="action" value="changeMemberInfoAd" class="btn btn-success">変更</button>
						<button type="submit" name="action" value="deleteMember" class="btn btn-danger">削除</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<%
		} else {
				out.println("<h2>不正なセッション</h2>");
				out.println("<p>セッションが切れた、もしくは不正であるためログインしなおしてください。</p>");
				out.println("<a href=\"./index.jsp\">ログインページ</a>");
			}
		} catch (NullPointerException e) {
			out.println("<h2>不正なセッション</h2>");
			out.println("<p>ログインをしてください。</p>");
			out.println("<a href=\"./index.jsp\">ログインページ</a>");
		}
	%>
	<style>
		dt, dd {
			margin-top: 10px;
			margin-bottom: 10px;
		}
	</style>

	<%@ include file="/include/foot.jsp" %>
</body>
</html>