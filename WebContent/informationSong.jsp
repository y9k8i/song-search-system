<%@page import="model.Song"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<!DOCTYPE html>
<html lang="ja">
<jsp:include page="/include/head.jsp" ><jsp:param name="title" value="楽曲詳細情報" /></jsp:include>
<body oncopy="return false;">
	<%
		try {
			if ((boolean) session.getAttribute("login")) {
				Song detail = (Song) session.getAttribute("detailSong");
	%>
	<%@ include file="/include/headBox.jsp" %>
	<%@ include file="/include/nav.jsp" %>

	<div class="container-fluid w-75">
		<h3>
			<span id="userID"><%=detail.getSongName()%></span>の楽曲情報
		</h3>
		<div>
			<dl class="row">
				<dt class="col-sm-2">歌手名</dt>
				<dd class="col-sm-4"><%=detail.getSinger()%></dd>
				<dt class="col-sm-2">曲名</dt>
				<dd class="col-sm-4"><%=detail.getSongName()%></dd>
				<dt class="col-sm-2">作詞者名</dt>
				<dd class="col-sm-4"><%=detail.getLyricist()%></dd>
				<dt class="col-sm-2">作曲者名</dt>
				<dd class="col-sm-4"><%=detail.getComposer()%></dd>
				<dt class="col-sm-2">番組名</dt>
				<dd class="col-sm-4"><%=
					detail.getProgramName().equals("none") ? "なし" : detail.getProgramName() %>
				</dd>
				<dt class="col-sm-2">ジャンル</dt>
				<dd class="col-sm-4"><%=detail.getGenreName()%></dd>
				<dt class="col-sm-2">歌詞</dt>
				<pre class="col-sm-10 mt-2"><%=detail.getLyrics()%></pre>
			</dl>
		</div>
	</div>

	<%
		if (session.getAttribute("admin") != null && (boolean) session.getAttribute("admin")) {
	%>
	<br>
	<div class="row justify-content-center">
		<form action="SearchController" method="post">
			<button type="submit" name="action" value="changeSong" class="btn btn-success">変更</button>
			<button type="submit" name="action" value="deleteSong" class="btn btn-danger">削除</button>
		</form>
	</div>
	<%
		}
			} else {
				out.println("<h2>不正なセッション</h2>");
				out.println("<p>セッションが切れた、もしくは不正であるためログインしなおしてください。</p>");
				out.println("<a href=\"./index.jsp\">ログインページ</a>");
			}
		} catch (NullPointerException e) {
			out.println("<h2>不正なセッション</h2>");
			out.println("<p>ログインをしてください。</p>");
			out.println("<a href=\"./index.jsp\">ログインページ</a>");
		}
	%>
	<style>
		dt, dd, textarea {
			margin-top: 10px;
			margin-bottom: 10px;
		}
	</style>

	<%@ include file="/include/foot.jsp" %>
</body>
</html>