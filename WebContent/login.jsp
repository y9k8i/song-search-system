<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<!DOCTYPE html>
<html lang="ja">
<jsp:include page="/include/head.jsp" ><jsp:param name="title" value="ログイン" /></jsp:include>
<body>
<div class="top">
	<div class="top-box">
		<span class="login-top-image">
			<span id="song-S">S</span>ONG<br><span id="search-S">S</span>EARCH<br><span id="sistem-S">S</span>YSTEM
		</span>
	</div>
</div>
<%
if(session.getAttribute("login") != null && !(Boolean)session.getAttribute("login")) {
	out.println("<p style=\"color: red\">ユーザ名またはパスワードが違います</p>");
} else if(session.getAttribute("admin") != null){
	session.invalidate();
}
%>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-lg-7">
				<h3 style="text-align:center">LOGIN</h3>
				<form action="UserController" method="post">
					<div class="form-group row">
						<label for="text3a" class="col-sm-3 col-form-label">ユーザID</label>
						<div class="col-sm-9">
							<input type="text" name="id" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<label for="text3b" class="col-sm-3 col-form-label">パスワード</label>
						<div class="col-sm-9">
							<input type="password" name="password" class="form-control">
						</div>
					</div>
					<div class="form-group row justify-content-center">
						<button type="submit" name="action" value="login" class="btn btn-primary">LOGIN</button>
					</div>
				</form>
			</div>
			<div class="col-md-4 col-lg-5 d-flex justify-content-center align-items-center">
				<div>
					<div class="row justify-content-center">
						<a href="./registerMember.jsp">
							<button type="submit" class="btn btn-primary">新規登録</button>
						</a>
					</div>
					<div class="row justify-content-center">
						<br>
						<p class="text-center">はじめての方はこちらから登録してください</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<style>
		.login-top-image {
			display: inline-block;
			font: 'メイリオ';
			background: #77e8ca;
			letter-spacing: 5px;
			line-height: 100px;
			color: #fff;
			font-size: 56px;
			padding: 0;
			margin: 0;
			font-weight: bold;
			user-select: none
		}
		.top-box {
			background: #77e8ca;
			width: 100%;
			padding-left: 30%;
		}
	</style>

	<%@ include file="/include/foot.jsp" %>
</body>
</html>