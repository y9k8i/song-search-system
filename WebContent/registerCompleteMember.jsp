<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<!DOCTYPE html>
<html lang="ja">
<jsp:include page="/include/head.jsp" ><jsp:param name="title" value="会員登録完了" /></jsp:include>
<body>
	<header>
	<%@ include file="/include/headBox.jsp" %>
	</header>

	<div class="container">
		<jsp:include page="/include/completeRow.jsp" >
			<jsp:param name="msg" value="会員登録が完了しました" />
			<jsp:param name="link" value="index.jsp" />
		</jsp:include>
	</div>

	<%@ include file="/include/foot.jsp" %>
</body>
</html>