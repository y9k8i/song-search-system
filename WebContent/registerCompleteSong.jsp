<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<!DOCTYPE html>
<html lang="ja">
<jsp:include page="/include/head.jsp" ><jsp:param name="title" value="楽曲情報登録完了" /></jsp:include>
<body>
	<header>
	<%@ include file="/include/headBox.jsp" %>
	</header>

	<div class="container">
		<jsp:include page="/include/completeRow.jsp" >
			<jsp:param name="msg" value="楽曲情報を登録しました" />
			<jsp:param name="link" value="top.jsp" />
		</jsp:include>
	</div>

	<%@ include file="/include/foot.jsp" %>
</body>
</html>