<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<!DOCTYPE html>
<html lang="ja">
<jsp:include page="/include/head.jsp" ><jsp:param name="title" value="確認画面" /></jsp:include>
<body>
	<header>
	<%@ include file="/include/headBox.jsp" %>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<button type="button" class="btn btn-warning" onclick="history.back()">戻る</button>
	</nav>
	</header>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-sm-6">
				<h2><span id="under">会員登録確認</span></h2>
				<dl class="row">
					<dt class="col-sm-5">ユーザID</dt>
					<dd class="col-sm-7"><%= session.getAttribute("newId") %></dd>
					<dt class="col-sm-5">パスワード</dt>
					<dd class="col-sm-7">(プライバシのため表示されません)</dd>
					<dt class="col-sm-5">ユーザネーム</dt>
					<dd class="col-sm-7"><%= session.getAttribute("newName") %></dd>
					<dt class="col-sm-5">年齢</dt>
					<dd class="col-sm-7"><%= session.getAttribute("newAge") %></dd>
					<dt class="col-sm-5">性別</dt>
					<dd class="col-sm-7"><%= session.getAttribute("newSex") %></dd>
				</dl>
				<div class="row justify-content-center">
					<form action="UserController" method="post">
						<button type="submit" name="action" value="regist" class="btn btn-success">登録する</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<style>
	h3{
		text-align: center;
	}
	dt,dd{
		margin-top: 10px;
		margin-bottom: 10px;
	}
	p{
		text-align: center;
		margin-bottom: 20px;
	}
	#ch-box{
		margin-right: 20px;
	}
	</style>

	<%@ include file="/include/foot.jsp" %>
</body>
</html>