<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<!DOCTYPE html>
<html lang="ja">
<jsp:include page="/include/head.jsp" ><jsp:param name="title" value="会員登録" /></jsp:include>
<body>
	<header>
	<%@ include file="/include/headBox.jsp" %>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<button type="button" class="btn btn-warning" onclick="history.back()">戻る</button>
	</nav>
	</header>
<%
if(session.getAttribute("caution") != null && (boolean)session.getAttribute("caution")){
	out.println("<p style=\"color: red\">入力に誤りがあります</p>");
	out.println("<p style=\"color: red\">指示通りに入力してください。</p>");
}
%>
	<div class="container">
		<h2><span id="under">会員登録</span></h2>
		<form action="UserController" method="post">
			<div class="form-group row">
				<label for="newId" class="col-sm-6 col-form-label">ユーザID（5～10文字）</label>
				<div class="col-sm-6">
					<input type="text" name="newId" class="form-control" minlength="5" maxlength="10" value="<%=
						(session.getAttribute("newId") != null) ? session.getAttribute("newId") : ""%>" id="newId" required>
				</div>
			</div>
			<div class="form-group row">
				<label for="newPassword" class="col-sm-6 col-form-label">パスワード（8～16文字）</label>
				<div class="col-sm-6">
					<input type="password" name="newPassword" id="newPassword" class="form-control" minlength="8" maxlength="16" required>
				</div>
			</div>
			<div class="form-group row">
				<label for="newName" class="col-sm-6 col-form-label">ユーザネーム（2～20文字）</label>
				<div class="col-sm-6">
					<input type="text" name="newName" class="form-control" minlength="2" maxlength="20" value="<%=
						(session.getAttribute("newName") != null) ? session.getAttribute("newName") : ""%>" id="newName" required>
				</div>
			</div>
			<div class="form-group row">
				<label for="newAge" class="col-sm-6 col-form-label">年齢</label>
				<div class="col-sm-6">
					<input type="number" name="newAge" class="form-control" value="<%=
						(session.getAttribute("newAge") != null) ? session.getAttribute("newAge") : ""%>" id="newAge" required>
				</div>
			</div>
			<div class="form-group row">
				<label for="text3b" class="col-sm-6 col-form-label">性別</label>
				<div class="col-sm-6">
					<label><input type="radio" name="newSex" value="man" required
						<%=(session.getAttribute("newSex") != null && session.getAttribute("newSex").equals("男性")) ? "checked=\"checked\"" : ""%>>男性</label>
					<label><input type="radio" name="newSex" value="woman"
						<%=(session.getAttribute("newSex") != null && session.getAttribute("newSex").equals("女性")) ? "checked=\"checked\"" : ""%>>女性</label>
				</div>
			</div>
			<div class="row justify-content-center">
				<button id="create" type="submit" name="action" value="checkSend" class="btn btn-success">登録する</button>
			</div>
		</form>
	</div>
	<style>
	h2{
		text-align: center;
	}
	#under{
		 border-bottom: solid 3px #87CEFA;
	}
	dt, dd{
		margin-top: 10px;
		margin-bottom: 10px;
	}
	</style>

	<%@ include file="/include/foot.jsp" %>
</body>
</html>