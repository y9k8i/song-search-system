<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<!DOCTYPE html>
<html lang="ja">
<jsp:include page="/include/head.jsp" ><jsp:param name="title" value="楽曲情報登録" /></jsp:include>
<body>
	<%
		try {
			if ((boolean) session.getAttribute("login")) {
	%>
	<%@ include file="/include/headBox.jsp" %>
	<%@ include file="/include/nav.jsp" %>

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-sm-8">
				<h3><span id="under">楽曲情報登録</span></h3>
				<p>歌手名、作曲者、作詞者は ';'または','で複数人入力できます。スペースで入力しないでください。*は必須</p>

	<%
				if (session.getAttribute("caution") != null && (boolean) session.getAttribute("caution")) {
					out.println("<p style=\"color: red\">入力に空欄や不正があります。</p>");
					session.setAttribute("caution", false);
				}
	%>

				<form action="SearchController" method="post">
					<dl class="row">
						<dt class="col-sm-5">*曲名</dt>
						<dd class="col-sm-7">
							<input type="text" name="newTitle" value="<%=(session.getAttribute("newTitle") != null) ?
								session.getAttribute("newTitle") : ""%>" class="form-control" required />
						</dd>
						<dt class="col-sm-5">*歌手名</dt>
						<dd class="col-sm-7">
							<input type="text" name="newSinger" value="<%=(session.getAttribute("newSinger") != null) ?
								session.getAttribute("newSinger") : ""%>" class="form-control" required />
						</dd>
						<dt class="col-sm-5">作曲者</dt>
						<dd class="col-sm-7">
							<input type="text" name="newComposer" value="<%=(session.getAttribute("newComposer") != null) ?
								session.getAttribute("newComposer") : ""%>" class="form-control" />
						</dd>
						<dt class="col-sm-5">作詞者</dt>
						<dd class="col-sm-7">
							<input type="text" name="newLyricist" value="<%=(session.getAttribute("newLyricist") != null) ?
								session.getAttribute("newLyricist") : ""%>" class="form-control" />
						</dd>
						<dt class="col-sm-5">番組名</dt>
						<dd class="col-sm-7">
							<input type="text" name="newProgram" value="<%=(session.getAttribute("newProgram") != null) ?
								session.getAttribute("newProgram") : ""%>" class="form-control" />
						</dd>
						<dt class="col-sm-5">*ジャンル</dt>
						<dd class="col-sm-7">
							<select name="newGenre" class="form-control" required>
								<option value="overall">総合</option>
								<option value="j-pop">J-POP</option>
								<option value="westan">洋楽</option>
								<option value="jazz">ジャズ</option>
								<option value="enka">演歌</option>
								<option value="k-pop">K-POP</option>
								<option value="japanese-rock">邦楽ロック</option>
								<option value="nursery-rhymes">童謡</option>
								<option value="anime">アニメ</option>
								<option value="vocaloid">ボーカロイド</option>
								<option value="game">ゲーム</option>
							</select>
						</dd>
						<dt class="col-sm-5">*歌詞</dt>
						<dd class="col-sm-7">
							<textarea cols="50" rows="10" name="newLyrics" class="form-control" required><%=
								(session.getAttribute("newLyrics") != null) ? session.getAttribute("newLyrics") : ""
							%></textarea>
						</dd>
					</dl>
					<div class="row justify-content-center">
						<button type="submit" name="action" value="checkSend" class="btn btn-success">登録確認</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<%
		} else {
				out.println("<h2>不正なセッション</h2>");
				out.println("<p>セッションが切れた、もしくは不正であるためログインしなおしてください。</p>");
				out.println("<a href=\"./index.jsp\">ログインページ</a>");
			}
		} catch (NullPointerException e) {
			out.println("<h2>不正なセッション</h2>");
			out.println("<p>ログインをしてください。</p>");
			out.println("<a href=\"./index.jsp\">ログインページ</a>");
		}
	%>
	<%@ include file="/include/foot.jsp" %>
</body>
</html>