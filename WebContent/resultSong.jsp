<%@page import="servlet.SearchHistoryController"%>
<%@page import="model.Song"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<!DOCTYPE html>
<html lang="ja">
<jsp:include page="/include/head.jsp" ><jsp:param name="title" value="検索結果" /></jsp:include>
<body>
	<%@ include file="/include/headBox.jsp" %>
	<%
		try {
			if ((boolean) session.getAttribute("login")) {
	%>
	<%@ include file="/include/nav.jsp" %>
	<%
				if ((boolean) session.getAttribute("search")) {
	%>
	<div class="container">
	<h3>
		<span id="searchWord"><%=session.getAttribute("searchWord")%></span>の検索結果一覧
	</h3>
	<div class="table-responsive">
	<table class="table table-hover">
		<thead>
		<tr>
			<th>#</th>
			<th>歌手名</th>
			<th>曲名</th>
			<th>ジャンル</th>
		</tr>
		</thead>
		<tbody>
	<%
					Song[] song = (Song[]) session.getAttribute("result");
					int i = 1;
					for (Song s : song) {
						out.print("<tr>");
						out.print("<td>" + i + "</td>");
						out.print("<td>" + s.getSinger() + "</td>");
						out.print(
							"<td><form action=\"SearchHistoryController\" method=\"post\" name=\"form" + i
								+ "\"><input type=\"hidden\" name=\"songid\" value=\""
								+ s.getSongID() + "\"><a href=\"javascript:form" + i + ".submit()\">"
								+ s.getSongName()
								+ "</a></form></td>");
						out.print("<td>" + s.getGenreName() + "</td>");
						out.print("</tr>");
						i++;
					}
	%>
		</tbody>
	</table>
	</div>
	</div>

	<%
					session.setAttribute("search", false);
				} else {
	%>
	<div class="container">
		<h3><%=session.getAttribute("searchWord")%>の検索結果一覧</h3>
		<p>検索結果はありませんでした。</p>
	</div>

	<%
		}
			} else {
				out.println("<h2>不正なセッション</h2>");
				out.println("<p>セッションが切れた、もしくは不正であるためログインしなおしてください。</p>");
				out.println("<a href=\"./index.jsp\">ログインページ</a>");
			}
		} catch (NullPointerException e) {
			out.println("<h2>不正なセッション</h2>");
			out.println("<p>ログインをしてください。</p>");
			out.println("<a href=\"./index.jsp\">ログインページ</a>");
		}
	%>

	<%@ include file="/include/foot.jsp" %>
</body>
</html>