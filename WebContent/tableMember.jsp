<%@page import="model.Member"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<!DOCTYPE html>
<html lang="ja">
<jsp:include page="/include/head.jsp" ><jsp:param name="title" value="会員一覧" /></jsp:include>
</head>
<body>
	<%
	try {
		if ((boolean) session.getAttribute("login") && (boolean) session.getAttribute("admin")) {
	%>
	<%@ include file="/include/headBox.jsp" %>
	<%@ include file="/include/nav.jsp" %>
	<div class="container">
		<h3><span id="under">会員一覧</span></h3>
	<%
			if (session.getAttribute("allMem") != null) {
	%>
		<div class="table-responsive">
		<div style="height:300px; overflow-y:scroll;">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>ユーザID</th>
						<th>年齢</th>
						<th>性別</th>
					</tr>
				</thead>
				<tbody>
	<%
				Member[] member = (Member[]) session.getAttribute("allMem");
				int i = 1;
				for (Member m : member) {
					out.print("<tr>");
					out.print("<td>" + i + "</td>");
					out.print(
						"<td><form action=\"UserController\" method=\"post\" name=\"form" + i
							+ "\"><input type=\"hidden\" name=\"userid\" value=\""
							+ m.getUserID() + "\"><a href=\"javascript:form" + i + ".submit()\">"
							+ m.getUserID()
							+ "</a></form></td>");
					out.print("<td>" + m.getAge() + "</td>");
					out.print("<td>" + m.getSex() + "</td>");
					out.print("</tr>");
					i++;
				}
	%>
				</tbody>
			</table>
		</div>
		</div>
<%
			} else {
				out.println("<h3 style=\"text-aglin:center\">検索結果を取得できませんでした。</h3>");
				out.println("<p>セッションが切れた、もしくは不正であるためログインしなおしてください。</p>");
				out.println("<a href=\"./index.jsp\">ログインページ</a>");
			}
			//session.setAttribute("allMem", null);
%>
	</div>
<%
		} else {
			out.println("<h2>不正なセッション</h2>");
			out.println("<p>セッションが切れた、もしくは不正であるためログインしなおしてください。</p>");
			out.println("<a href=\"./index.jsp\">ログインページ</a>");
		}
	} catch (NullPointerException e) {
		out.println("<h2>不正なセッション</h2>");
		out.println("<p>ログインをしてください。</p>");
		out.println("<a href=\"./index.jsp\">ログインページ</a>");
	}
%>
	<style>
		h3 {
			text-align: center;
		}
	</style>

	<%@ include file="/include/foot.jsp" %>
</body>
</html>