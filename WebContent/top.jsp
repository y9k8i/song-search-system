<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>楽曲検索システム（SSS）</title>
<%--CSS読み込み--%>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css" type="text/css">
<%--戻るボタン非表示&イベント無効化--%>
<style>
	.btn-warning {
		opacity: 0;
		pointer-events: none;
	}
</style>
</head>
<body>
	<%@ include file="/include/headBox.jsp" %>
	<%
		try {
			if ((boolean) session.getAttribute("login")) {
	%>
	<%@ include file="/include/nav.jsp" %>
	<%
				if (session.getAttribute("cautionS") != null && (boolean) session.getAttribute("cautionS")) {
	%>

	<p style="color: red">何かキーワードを入力してください。</p>
	<%
					session.setAttribute("cautionS", false);
				}
	%>
	<div class="container">
		<div class="row">
			<div class="col-md-5 align-self-center">
				<img src="guitar.jpg" class="img-fluid">
			</div>
			<div class="col-md-7">
				<h3>楽曲を探す</h3>
				<p>下の欄に1つ以上キーワードを入れて検索　複数のもので絞ることもできます</p>
				<form action="SearchController" method="post">
					<div class="form-group">
						<label for="searchSongName">曲名</label>
						<input type="text" name="songName" class="form-control" id="searchSongName" placeholder="">
					</div>
					<div class="form-group">
						<label for="searchSinger">歌手名</label>
						<input type="text" name="singer" class="form-control" id="searchSinger" placeholder="">
					</div>
					<div class="form-group">
						<label for="searchProgramName">番組名</label>
						<input type="text" name="programName" class="form-control" id="searchProgramName" placeholder="">
					</div>
					<div class="form-group">
						<label for="searchGenreName">ジャンル</label>
						<select name="genreName" class="form-control" id="searchGenreName">
							<option value="">指定する場合は選択</option>
							<option value="overall">総合</option>
							<option value="j-pop">J-POP</option>
							<option value="westan">洋楽</option>
							<option value="jazz">ジャズ</option>
							<option value="enka">演歌</option>
							<option value="k-pop">K-POP</option>
							<option value="japanese-rock">邦楽ロック</option>
							<option value="nursery-rhymes">童謡</option>
							<option value="anime">アニメ</option>
							<option value="vocaloid">ボーカロイド</option>
							<option value="game">ゲーム</option>
						</select>
					</div>
					<div class="form-group">
						<label for="searchLyrics">歌詞 (一部入力で可)</label>
						<input type="text" name="lyrics" class="form-control" id="searchLyrics" placeholder="">
					</div>
					<button type="submit" name="action" value="search" class="btn btn-primary mb-2">曲を探す</button>
				</form>
			</div>
		</div>
	</div>
	<%
			} else {
				out.println("<h2>不正なセッション</h2>");
				out.println("<p>セッションが切れた、もしくは不正であるためログインしなおしてください。</p>");
				out.println("<a href=\"./index.jsp\">ログインページ</a>");
			}
		} catch (NullPointerException e) {
			out.println("<h2>不正なセッション</h2>");
			out.println("<p>ログインをしてください。</p>");
			out.println("<a href=\"./index.jsp\">ログインページ</a>");
		}
	%>

	<%@ include file="/include/foot.jsp" %>
</body>
</html>