package model;

import java.io.File;
import java.util.ResourceBundle;

public class DAOConstants {
	// Docker内部で動作している場合trueになる
	static final boolean dockerenv = new File("/.dockerenv").exists();

	static final String dbname = "SSS"; // PostgreSQL DB name
	static final String sqlHostname = dockerenv ? "pgs_SSS" : "localhost";
	static final String driverClassName = "org.postgresql.Driver";

	static final ResourceBundle rb = ResourceBundle.getBundle("db_key");
	static final String user = rb.getString("user");
	static final String password = dockerenv ? System.getenv("POSTGRES_PASSWORD") : rb.getString("password");
	static final String url = "jdbc:postgresql://" + sqlHostname + "/" + dbname;
}
