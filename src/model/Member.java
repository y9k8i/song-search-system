package model;

import java.io.Serializable;

public class Member implements Serializable{

	private static final long serialVersionUID = 1L;
	private String userID;
	private String userName;
	private String password;
	private int age;
	private String sex;

	public Member() {
		userID = "";
		userName = "";
		password = "";
		age = -1;
		sex = "";
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}


}
