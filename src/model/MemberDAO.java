package model;

import static model.DAOConstants.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MemberDAO {

	private Logger logger = LoggerFactory.getLogger(MemberDAO.class);

	public Member findMemberID(String userID, String passwordHash) throws SQLException {
		Member getedMember = new Member();
		Connection connection;
		String sql = "SELECT * FROM member WHERE userid=? and password=?";

		try {
			Class.forName(driverClassName);
			connection = DriverManager.getConnection(url, user, password);
			PreparedStatement pstmt = connection.prepareStatement(sql);

			pstmt.setString(1, userID);
			pstmt.setString(2, passwordHash);

			ResultSet resultSet = pstmt.executeQuery();
			if (resultSet.next()) {
				getedMember.setUserID(resultSet.getString("userid"));
				getedMember.setPassword(resultSet.getString("password"));
				getedMember.setUserName(resultSet.getString("username"));
				getedMember.setAge(Integer.parseInt(resultSet.getString("age")));
				getedMember.setSex(resultSet.getString("sex"));
			} else {
				resultSet.close();
				connection.close();
				return getedMember;
			}

			resultSet.close();
			connection.close();
		} catch (Exception e) {
			logger.error("例外発生", e);
		}

		return getedMember;
	}

	public void updateMember(String userID, String userName, String passwordHash, int age, String sex, boolean newUser,
			Member oldData) throws SQLException {
		Connection connection;
		String sql = "";

		try {
			Class.forName(driverClassName);
			connection = DriverManager.getConnection(url, user, password);
			PreparedStatement pstmt;

			if (newUser) {
				sql = "INSERT INTO member VALUES(?, ?, ?, ?, ?)";
				pstmt = connection.prepareStatement(sql);

				pstmt.setString(1, userID);
				pstmt.setString(2, userName);
				pstmt.setString(3, passwordHash);
				pstmt.setInt(4, age);
				pstmt.setString(5, sex);

				pstmt.executeUpdate();
			} else {
				sql = "";

				//userID変更時
				if (userID != null && !userID.equals("")) {
					sql = "UPDATE member SET userid=? WHERE username=?";
					pstmt = connection.prepareStatement(sql);

					pstmt.setString(1, userID);
					pstmt.setString(2, oldData.getUserName());
					pstmt.executeUpdate();
				} else {
					userID = oldData.getUserID();
				}
				//ユーザネーム変更時
				if (userName != null && !userName.equals("")) {
					sql = "UPDATE member SET username=? WHERE userid=?";
					pstmt = connection.prepareStatement(sql);

					pstmt.setString(1, userName);
					pstmt.setString(2, userID);
					pstmt.executeUpdate();
				}
				//パスワード変更時
				if (passwordHash != null && !passwordHash.equals("")) {
					sql = "UPDATE member SET password=? WHERE userid=?";
					pstmt = connection.prepareStatement(sql);

					pstmt.setString(1, passwordHash);
					pstmt.setString(2, userID);
					pstmt.executeUpdate();
				}
				//年齢変更時
				if (age != -1) {
					sql = "UPDATE member SET age=? WHERE userid=?";
					pstmt = connection.prepareStatement(sql);

					pstmt.setInt(1, age);
					pstmt.setString(2, userID);
					pstmt.executeUpdate();
				}
				//性別変更時
				if (sex != null && !sex.equals("")) {
					sql = "UPDATE member SET sex = ? WHERE userid=?";
					pstmt = connection.prepareStatement(sql);

					pstmt.setString(1, sex);
					pstmt.setString(2, userID);
					pstmt.executeUpdate();
				}
			}

			connection.close();
		} catch (Exception e) {
			logger.error("例外発生", e);
		}
	}

	/*
	 * 追加要素（ユーザーの一覧を要求する）
	 */
	public ArrayList<Member> returnMemberAll(String userID) throws SQLException {
		ArrayList<Member> listMem = new ArrayList<>();
		Connection connection;
		String sql = "SELECT * FROM member ORDER BY userid ASC";

		try {
			Class.forName(driverClassName);
			connection = DriverManager.getConnection(url, user, password);
			PreparedStatement pstmt = connection.prepareStatement(sql);

			ResultSet resultSet = pstmt.executeQuery();
			while (resultSet.next()) {
				Member getedMember = new Member();
				getedMember.setUserID(resultSet.getString("userid"));
				getedMember.setPassword(resultSet.getString("password"));
				getedMember.setUserName(resultSet.getString("username"));
				getedMember.setAge(Integer.parseInt(resultSet.getString("age")));
				getedMember.setSex(resultSet.getString("sex"));
				if (getedMember.getUserID().equals(userID) || getedMember.getUserID().length() < 5)
					continue;
				else
					listMem.add(getedMember);
			}

			resultSet.close();
			connection.close();
		} catch (ClassNotFoundException e) {
			logger.error("例外発生", e);
		}

		return listMem;
	}

	/*
	 * 追加要素（管理者用で、1ユーザーのデータを要求する）
	 */
	public Member returnMember(String userID) throws SQLException {
		Member getedMember = new Member();
		Connection connection;
		String sql = "SELECT * FROM member WHERE userid=?";

		try {
			Class.forName(driverClassName);
			connection = DriverManager.getConnection(url, user, password);
			PreparedStatement pstmt = connection.prepareStatement(sql);
			pstmt.setString(1, userID);

			ResultSet resultSet = pstmt.executeQuery();
			if (resultSet.next()) {
				getedMember.setUserID(resultSet.getString("userid"));
				getedMember.setPassword(resultSet.getString("password"));
				getedMember.setUserName(resultSet.getString("username"));
				getedMember.setAge(Integer.parseInt(resultSet.getString("age")));
				getedMember.setSex(resultSet.getString("sex"));
			} else {
				return null;
			}

			resultSet.close();
			connection.close();
		} catch (ClassNotFoundException e) {
			logger.error("例外発生", e);
		}

		return getedMember;
	}

	public void deleteMember(String userID) throws SQLException {
		Connection connection;
		String sql = "DELETE FROM member WHERE userid=?";
		try {
			Class.forName(driverClassName);
			connection = DriverManager.getConnection(url, user, password);
			PreparedStatement pstmt = connection.prepareStatement(sql);

			pstmt = connection.prepareStatement(sql);

			pstmt.setString(1, userID);
			pstmt.executeUpdate();

			connection.close();
		} catch (ClassNotFoundException e) {
			logger.error("例外発生", e);
			return;
		}
	}
}
