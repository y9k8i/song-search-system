package model;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MemberManager {

	private Member member = new Member();
	private Logger logger = LoggerFactory.getLogger(MemberManager.class);

	public boolean login(String userID, String password, String parameter) {
		String id = userID;
		String pass = password;
		String sha256 = "";

		if (parameter.equals("newCheck")) {
			try {
				MessageDigest digest = MessageDigest.getInstance("SHA-256");
				byte[] result = digest.digest(pass.getBytes());
				sha256 = String.format("%040x", new BigInteger(1, result));
			} catch (Exception e) {
				logger.error("例外発生", e);
				return false;
			}
		} else if (parameter.equals("check")) {
			sha256 = password;
		} else {
			return false;
		}

		MemberDAO mDao = new MemberDAO();
		Member memberGet = new Member();
		try {
			memberGet = mDao.findMemberID(id, sha256);
		} catch (SQLException e) {
			logger.error("例外発生", e);
			return false;
		}

		if (id.equals(memberGet.getUserID()) && sha256.equals(memberGet.getPassword())) {
			member.setUserID(memberGet.getUserID());
			member.setPassword(memberGet.getPassword());
			member.setUserName(memberGet.getUserName());
			member.setAge(memberGet.getAge());
			member.setSex(memberGet.getSex());
			return true;
		} else {
			return false;
		}
	}

	/*
	 * 新規登録時チェックメソッド（追加項目）
	 * 入力条件を満たしているのかをここで判定する。
	 */
	public boolean checkMember(String userID, String userName, String password, int age, String sex) {

		String msg = sex;
		String pass = password;
		String sha256 = "";

		if (pass.length() >= 8 && pass.length() <= 16) {
			//パスワードのハッシュ化
			try {
				MessageDigest digest = MessageDigest.getInstance("SHA-256");
				byte[] result = digest.digest(pass.getBytes());
				sha256 = String.format("%040x", new BigInteger(1, result));
			} catch (Exception e) {
				logger.error("例外発生", e);
				sha256 = "-1";
				return false;
			}
		} else {
			return false;
		}

		//性別判定
		if (msg != null && msg.equals("man")) {
			member.setSex("男性");
		} else if (msg != null && msg.equals("woman")) {
			member.setSex("女性");
		} else {
			return false;
		}

		//ユーザIDの文字数制限判定
		if (userID.length() > 4 && userID.length() <= 10) {
			member.setUserID(userID);
		} else {
			return false;
		}

		//ユーザネームの文字数制限判定
		if (userName.length() > 0 && userName.length() <= 20) {
			member.setUserName(userName);
		} else {
			return false;
		}

		//年齢判定
		if (age > 0 && age < 200) {
			member.setAge(age);
		} else {
			return false;
		}

		member.setPassword(sha256);
		return true;
	}

	public void registerMember(String userID, String userName, String password, int age, String sex) {
		//データベース登録
		MemberDAO mDao = new MemberDAO();
		try {
			mDao.updateMember(userID, userName, password, age, sex, true, null);
		} catch (SQLException e) {
			logger.error("例外発生", e);
		}
	}

	public void changeMember(String userID, String userName, String password, int age, String sex, Member old) {
		MemberDAO mDao = new MemberDAO();
		try {
			mDao.updateMember(userID, userName, password, age, sex, false, old);
		} catch (SQLException e) {
			logger.error("例外発生", e);
		}
	}

	public void deleteMember(String userID) {
		MemberDAO mDao = new MemberDAO();
		try {
			mDao.deleteMember(userID);
		} catch (SQLException e) {
			logger.error("例外発生", e);
		}
	}

	/*
	 * 追加要素
	 */
	public Member[] returnMembers(String userID, String password) {
		MemberDAO mDao = new MemberDAO();
		ArrayList<Member> tmpMem = new ArrayList<>();
		Member[] allMember = null;

		if(login(userID, password, "check")) {
			try {
				tmpMem = mDao.returnMemberAll(userID);
				allMember = (Member[]) tmpMem.toArray(new Member[tmpMem.size()]);
			} catch (SQLException e) {
				logger.error("例外発生", e);
				return allMember;
			}
		}
		return allMember;
	}

	/*
	 * 追加要素（1つの詳細データをとるため用）
	 */
	public Member returnMember(String userID, String password, String searchID) {
		MemberDAO mDao = new MemberDAO();
		Member tmember = null;

		if(login(userID, password, "check")) {
			try {
				tmember = mDao.returnMember(searchID);

			} catch (SQLException e) {
				logger.error("例外発生", e);
				return null;
			}
		} else {
			return null;
		}
		return tmember;
	}

	public Member getMember() {
		return member;
	}

}
