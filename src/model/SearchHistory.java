package model;

public class SearchHistory {

	private String userID;
	private String songID;
	private int searchDate;

	public SearchHistory() {
		userID = "";
		songID = "";
		searchDate = -1;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getSongID() {
		return songID;
	}

	public void setSongID(String songID) {
		this.songID = songID;
	}

	public int getSearchDate() {
		return searchDate;
	}

	public void setSearchDate(int searchDate) {
		this.searchDate = searchDate;
	}

}
