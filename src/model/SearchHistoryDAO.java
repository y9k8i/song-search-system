package model;

import static model.DAOConstants.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchHistoryDAO {

	private Logger logger = LoggerFactory.getLogger(SearchHistoryDAO.class);

	public ArrayList<SearchHistory> historyReturn(String userID) throws SQLException {
		ArrayList<SearchHistory> resultSearchHists = new ArrayList<>();
		Connection connection;
		String sql = "SELECT * FROM searchhistory WHERE userid=? ORDER BY searchdate DESC";

		try {
			Class.forName(driverClassName);
			connection = DriverManager.getConnection(url, user, password);

			PreparedStatement pstmt = connection.prepareStatement(sql);
			pstmt.setString(1, userID);
			ResultSet resultSet = pstmt.executeQuery();

			while (resultSet.next()) {
				SearchHistory resultHist = new SearchHistory();
				resultHist.setUserID(resultSet.getString("userid"));
				resultHist.setSongID(resultSet.getString("songid"));
				resultHist.setSearchDate(
						Integer.parseInt(new SimpleDateFormat("yyyyMMdd").format(resultSet.getDate("searchdate"))));

				resultSearchHists.add(resultHist);
			}

			resultSet.close();
			connection.close();
		} catch (ClassNotFoundException e) {
			logger.error("例外発生", e);
		}
		return resultSearchHists;
	}

	public void updateHistory(String userID, int songID, Date searchDate) throws SQLException {
		Connection connection;
		String sql = "SELECT * FROM searchhistory WHERE songid=? AND userid=?";
		java.sql.Date date = new java.sql.Date(searchDate.getTime());

		try {
			Class.forName(driverClassName);
			connection = DriverManager.getConnection(url, user, password);

			PreparedStatement pstmtS = connection.prepareStatement(sql);
			PreparedStatement pstmt;
			pstmtS.setInt(1, songID);
			pstmtS.setString(2, userID);

			ResultSet resultSet = pstmtS.executeQuery();
			if (resultSet.next()) {//引っかかるということは過去にみた履歴がある
				//日付のみ更新
				sql = "UPDATE searchhistory SET searchdate=? WHERE songid=? AND userid=?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setDate(1, date);
				pstmt.setInt(2, songID);
				pstmt.setString(3, userID);

				pstmt.executeUpdate();
			} else {//引っかからないので新規登録
				sql = "INSERT INTO searchhistory VALUES(?, ?, ?)";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, userID);
				pstmt.setInt(2, songID);
				pstmt.setDate(3, date);

				pstmt.executeUpdate();
			}

			resultSet.close();
			connection.close();
		} catch (ClassNotFoundException e) {
			logger.error("例外発生", e);
		}
	}
}
