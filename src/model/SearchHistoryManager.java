package model;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchHistoryManager {

	private Song getSong = new Song();
	private Song[] getSongs;
	private Logger logger = LoggerFactory.getLogger(SearchHistoryManager.class);

	public void registerHistory(String userID, String songID) {

		SearchHistoryDAO shmDAO = new SearchHistoryDAO();
		SongDAO sDao = new SongDAO();
		ArrayList<Song> songs;
		Song[] song;

		try {
			songs = sDao.songReturn(Integer.parseInt(songID), "", "", "", "", "", "", "");
			song = (Song[]) songs.toArray(new Song[songs.size()]);
			getSong = song[0];
		} catch (SQLException e) {
			logger.error("例外発生", e);
		}

		//日付の取得
		Date today = Calendar.getInstance().getTime();

		try {
			shmDAO.updateHistory(userID, Integer.parseInt(songID), today);
		} catch (SQLException e) {
			logger.error("例外発生", e);
		}
	}

	public SearchHistory[] searchHistory(String userID) {
		ArrayList<SearchHistory> hist = new ArrayList<>();
		ArrayList<Song> tmpSong = new ArrayList<>();
		SongDAO sDao = new SongDAO();
		SearchHistory[] histArray = null;
		SearchHistoryDAO shmDAO = new SearchHistoryDAO();

		try {
			hist = shmDAO.historyReturn(userID);
			histArray = (SearchHistory[]) hist.toArray(new SearchHistory[hist.size()]);
			if(histArray != null) {
				for(int i = 0; i < histArray.length; i++) {
					ArrayList<Song> songs;
					songs = sDao.songReturn(Integer.parseInt(histArray[i].getSongID()), "", "", "", "", "", "", "");
					tmpSong.add(songs.get(0));//一意に決まるので初めから先頭のみを指定
				}
				getSongs = (Song[]) tmpSong.toArray(new Song[tmpSong.size()]);
			}
			else {
				return histArray;
			}
		} catch (SQLException e) {
			logger.error("例外発生", e);
		}
		return histArray;
	}

	public Song getSong() {
		return getSong;
	}
	public Song[] getSongs() {
		return getSongs;
	}
}
