package model;

import static model.DAOConstants.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SongDAO {

	private Logger logger = LoggerFactory.getLogger(SongDAO.class);

	public ArrayList<Song> songReturn(int songID, String songName, String programName, String genreName, String lyrics,
			String singer, String composer, String lyricist) throws SQLException {

		ArrayList<Song> resultSongs = new ArrayList<>();
		Connection connection;
		String sql = "";
		int searchType = 0;

		try {
			Class.forName(driverClassName);
			connection = DriverManager.getConnection(url, user, password);
			PreparedStatement pstmt;
			String isongName = "";
			String iprogramName = "";
			String ilyrics = "";
			String isinger = "";

			if (songID != -1) { //idによるサーチは一意に決まるためそのほかと組み合わせない
				searchType = 1;
			} else {//その他は組み合わせて詳細検索を可能に
				if (songName != null && !songName.equals("") && !songName.equals("%") && !songName.contains("%%")) {
					searchType += 2;
					isongName = "%" + songName + "%";
				}
				if (programName != null && !programName.equals("") && !programName.equals("%")
						&& !programName.contains("%%")) {
					searchType += 4;
					iprogramName = programName + "%";
				}
				if (genreName != null && !genreName.equals("") && !genreName.equals("%") && !genreName.contains("%%")) {
					searchType += 8;
				}
				if (lyrics != null && !lyrics.equals("") && !lyrics.equals("%") && !lyrics.contains("%%")) {
					searchType += 16;
					ilyrics = lyrics + "%";
				}
				if (singer != null && !singer.equals("") && !singer.equals("%") && !singer.contains("%%")) {
					searchType += 32;
					isinger = "%" + singer + "%";
				}
			}

			switch (searchType) {
			case 1:
				sql = "SELECT * FROM v_song WHERE songid=?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setInt(1, songID);
				break;
			case 2:
				sql = "SELECT * FROM v_song WHERE songname ILIKE ?";
				pstmt = connection.prepareStatement(sql);

				pstmt.setString(1, isongName);
				break;
			case 4:
				sql = "SELECT * FROM v_song WHERE programname ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, iprogramName);
				break;
			case 6:
				sql = "SELECT * FROM v_song WHERE songname ILIKE ? AND programname ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, isongName);
				pstmt.setString(2, iprogramName);
				break;
			case 8:
				sql = "SELECT * FROM v_song WHERE genrename=?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, genreName);
				break;
			case 10:
				sql = "SELECT * FROM v_song WHERE songname ILIKE ? AND genrename=?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, isongName);
				pstmt.setString(2, genreName);
				break;
			case 12:
				sql = "SELECT * FROM v_song WHERE programname ILIKE ? AND genrename=?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, iprogramName);
				pstmt.setString(2, genreName);
				break;
			case 14:
				sql = "SELECT * FROM v_song WHERE songname ILIKE ? AND programname ILIKE ? AND genrename=?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, isongName);
				pstmt.setString(2, iprogramName);
				pstmt.setString(3, genreName);
				break;
			case 16:
				sql = "SELECT * FROM v_song WHERE lyrics ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, ilyrics);
				break;
			case 18:
				sql = "SELECT * FROM v_song WHERE songname ILIKE ? AND lyrics ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, isongName);
				pstmt.setString(2, ilyrics);
				break;
			case 20:
				sql = "SELECT * FROM v_song WHERE programname ILIKE ? AND lyrics ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, iprogramName);
				pstmt.setString(2, ilyrics);
				break;
			case 22:
				sql = "SELECT * FROM v_song WHERE songname ILIKE ? AND programname ILIKE ? AND lyrics ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, isongName);
				pstmt.setString(2, iprogramName);
				pstmt.setString(3, ilyrics);
				break;
			case 24:
				sql = "SELECT * FROM v_song WHERE genrename=? AND lyrics ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, genreName);
				pstmt.setString(2, ilyrics);
				break;
			case 26:
				sql = "SELECT * FROM v_song WHERE songname ILIKE ? AND genrename=? AND lyrics ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, isongName);
				pstmt.setString(2, genreName);
				pstmt.setString(3, ilyrics);
				break;
			case 28:
				sql = "SELECT * FROM v_song WHERE programname ILIKE ? AND genrename=? AND lyrics ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, iprogramName);
				pstmt.setString(2, genreName);
				pstmt.setString(3, ilyrics);
				break;
			case 30:
				sql = "SELECT * FROM v_song WHERE songname ILIKE ? AND programname ILIKE ? AND genrename=? AND lyrics ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, isongName);
				pstmt.setString(2, iprogramName);
				pstmt.setString(3, genreName);
				pstmt.setString(4, ilyrics);
				break;
			case 32:
				sql = "SELECT * FROM v_song WHERE singer ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, isinger);
				break;
			case 34:
				sql = "SELECT * FROM v_song WHERE songname ILIKE ? AND singer ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, isongName);
				pstmt.setString(2, isinger);
				break;
			case 36:
				sql = "SELECT * FROM v_song WHERE programname ILIKE ? AND singer ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, iprogramName);
				pstmt.setString(2, isinger);
				break;
			case 38:
				sql = "SELECT * FROM v_song WHERE songname ILIKE ? AND programname ILIKE ? AND singer ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, isongName);
				pstmt.setString(2, iprogramName);
				pstmt.setString(3, isinger);
				break;
			case 40:
				sql = "SELECT * FROM v_song WHERE genrename=? AND singer ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, genreName);
				pstmt.setString(2, isinger);
				break;
			case 42:
				sql = "SELECT * FROM v_song WHERE songname ILIKE ? AND genrename=? AND singer ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, isongName);
				pstmt.setString(2, genreName);
				pstmt.setString(3, isinger);
				break;
			case 44:
				sql = "SELECT * FROM v_song WHERE programname ILIKE ? AND genrename=? AND singer ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, iprogramName);
				pstmt.setString(2, genreName);
				pstmt.setString(3, isinger);
				break;
			case 46:
				sql = "SELECT * FROM v_song WHERE songname ILIKE ? AND programname ILIKE ? AND genrename=? AND singer ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, isongName);
				pstmt.setString(2, iprogramName);
				pstmt.setString(3, genreName);
				pstmt.setString(4, isinger);
				break;
			case 48:
				sql = "SELECT * FROM v_song WHERE lyrics ILIKE ? AND singer ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, ilyrics);
				pstmt.setString(2, isinger);
				break;
			case 50:
				sql = "SELECT * FROM v_song WHERE songname ILIKE ? AND lyrics ILIKE ? AND singer ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, isongName);
				pstmt.setString(2, ilyrics);
				pstmt.setString(3, isinger);
				break;
			case 52:
				sql = "SELECT * FROM v_song WHERE programname ILIKE ? AND lyrics ILIKE ? AND singer ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, iprogramName);
				pstmt.setString(2, ilyrics);
				pstmt.setString(3, isinger);
				break;
			case 54:
				sql = "SELECT * FROM v_song WHERE songname ILIKE ? AND programname ILIKE ? AND lyrics ILIKE ? AND singer ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, isongName);
				pstmt.setString(2, iprogramName);
				pstmt.setString(3, ilyrics);
				pstmt.setString(4, isinger);
				break;
			case 56:
				sql = "SELECT * FROM v_song WHERE genrename=? AND lyrics ILIKE ? AND singer ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, genreName);
				pstmt.setString(2, ilyrics);
				pstmt.setString(3, isinger);
				break;
			case 58:
				sql = "SELECT * FROM v_song WHERE songname ILIKE ? AND genrename=? AND lyrics ILIKE ? AND singer ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, isongName);
				pstmt.setString(2, genreName);
				pstmt.setString(3, ilyrics);
				pstmt.setString(4, isinger);
				break;
			case 60:
				sql = "SELECT * FROM v_song WHERE programname ILIKE ? AND genrename=? AND lyrics ILIKE ? AND singer ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, iprogramName);
				pstmt.setString(2, genreName);
				pstmt.setString(3, ilyrics);
				pstmt.setString(4, isinger);
				break;
			case 62:
				sql = "SELECT * FROM v_song WHERE songname ILIKE ? AND programname ILIKE ? AND genrename=? AND lyrics ILIKE ? AND singer ILIKE ?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, isongName);
				pstmt.setString(2, iprogramName);
				pstmt.setString(3, genreName);
				pstmt.setString(4, ilyrics);
				pstmt.setString(5, isinger);
				break;
			default:
				pstmt = connection.prepareStatement(sql);
				break;
			}

			ResultSet resultSet = pstmt.executeQuery();
			while (resultSet.next()) {
				Song resultSong = new Song();
				resultSong.setSongID(resultSet.getString("songid"));
				resultSong.setSongName(resultSet.getString("songname"));
				resultSong.setProgramName(resultSet.getString("programName"));
				resultSong.setGenreName(resultSet.getString("genrename"));
				resultSong.setLyrics(resultSet.getString("lyrics"));
				resultSong.setSinger(resultSet.getString("singer"));
				resultSong.setComposer(resultSet.getString("composer"));
				resultSong.setLyricist(resultSet.getString("lyricist"));

				resultSongs.add(resultSong);
			}

			resultSet.close();
			connection.close();
		} catch (ClassNotFoundException e) {
			logger.error("例外発生", e);
		}

		return resultSongs;
	}

	public void updateSong(int songID, String songName, String programName, String genreName, String lyrics,
			String[] singer, String[] composer, String[] lyricist, boolean newSong) throws SQLException {

		Connection connection;
		String sql = "";

		try {
			Class.forName(driverClassName);
			connection = DriverManager.getConnection(url, user, password);

			PreparedStatement pstmt;
			int gSongNameId;
			int gProgramNameId;
			int gGenreId;
			int[] gSingerId;
			int[] gComposerId;
			int[] gLyricistId;
			int gSongid;

			if (newSong) {//新規登録（曲名、番組名、歌手、作曲、作詞、楽曲、音楽家と楽曲との結び付けの順に行う）
				//新規登録時には音楽家は書かれた分だけ検証するためここで初期化
				gSingerId = new int[singer.length];
				gComposerId = new int[composer.length];
				gLyricistId = new int[lyricist.length];

				sql = "SELECT * FROM songname WHERE songname=?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, songName);

				ResultSet resultSet = pstmt.executeQuery();
				if (!resultSet.next()) { //曲名がないときはまず追加
					sql = "INSERT INTO songname VALUES(nextval('songname_songnameid_seq'), ?)";
					pstmt = connection.prepareStatement(sql);
					pstmt.setString(1, songName);
					pstmt.executeUpdate();

					sql = "SELECT currval('songname_songnameid_seq')";
					pstmt = connection.prepareStatement(sql);
					resultSet = pstmt.executeQuery();
					resultSet.next();
					gSongNameId = resultSet.getInt("currval");
				} else {
					gSongNameId = resultSet.getInt("songnameid");
				}

				if (programName.equals("")) { //番組名なしはid=0に固定
					gProgramNameId = 0;
				} else {
					sql = "SELECT * FROM programname WHERE programname=?";
					pstmt = connection.prepareStatement(sql);
					pstmt.setString(1, programName);

					resultSet = pstmt.executeQuery();
					if (!resultSet.next()) { //番組名がなければ追加しよう
						sql = "INSERT INTO programname VALUES(nextval('programname_programnameid_seq'), ?)";
						pstmt = connection.prepareStatement(sql);
						pstmt.setString(1, programName);
						pstmt.executeUpdate();

						sql = "SELECT currval('programname_programnameid_seq')";
						pstmt = connection.prepareStatement(sql);
						resultSet = pstmt.executeQuery();
						resultSet.next();
						gProgramNameId = resultSet.getInt("currval");
					} else {
						gProgramNameId = resultSet.getInt("programnameid");
					}
				}

				//歌手*****************************************
				for (int i = 0; i < singer.length; i++) {
					sql = "SELECT * FROM musician WHERE name=?";
					pstmt = connection.prepareStatement(sql);
					pstmt.setString(1, singer[i]);

					resultSet = pstmt.executeQuery();
					if (!resultSet.next()) { //歌手名がなければ追加
						sql = "INSERT INTO musician VALUES(nextval('musician_musicianid_seq'), ?)";
						pstmt = connection.prepareStatement(sql);
						pstmt.setString(1, singer[i]);
						pstmt.executeUpdate();

						sql = "SELECT currval('musician_musicianid_seq')";
						pstmt = connection.prepareStatement(sql);
						resultSet = pstmt.executeQuery();
						resultSet.next();
						gSingerId[i] = resultSet.getInt("currval");
					} else {
						gSingerId[i] = resultSet.getInt("musicianid");
					}
				}
				//*********************************************

				//作曲者***************************************
				for (int j = 0; j < composer.length; j++) {
					sql = "SELECT * FROM musician WHERE name=?";
					pstmt = connection.prepareStatement(sql);
					pstmt.setString(1, composer[j]);

					resultSet = pstmt.executeQuery();
					if (!resultSet.next()) { //作曲者がなければ追加
						sql = "INSERT INTO musician VALUES(nextval('musician_musicianid_seq'), ?)";
						pstmt = connection.prepareStatement(sql);
						pstmt.setString(1, composer[j]);
						pstmt.executeUpdate();

						sql = "SELECT currval('musician_musicianid_seq')";
						pstmt = connection.prepareStatement(sql);
						resultSet = pstmt.executeQuery();
						resultSet.next();
						gComposerId[j] = resultSet.getInt("currval");
					} else {
						gComposerId[j] = resultSet.getInt("musicianid");
					}
				}
				//*********************************************

				//作詞者***************************************
				for (int k = 0; k < lyricist.length; k++) {
					sql = "SELECT * FROM musician WHERE name=?";
					pstmt = connection.prepareStatement(sql);
					pstmt.setString(1, lyricist[k]);

					resultSet = pstmt.executeQuery();
					if (!resultSet.next()) { //作詞者がなければ追加
						sql = "INSERT INTO musician VALUES(nextval('musician_musicianid_seq'), ?)";
						pstmt = connection.prepareStatement(sql);
						pstmt.setString(1, lyricist[k]);
						pstmt.executeUpdate();

						sql = "SELECT currval('musician_musicianid_seq')";
						pstmt = connection.prepareStatement(sql);
						resultSet = pstmt.executeQuery();
						resultSet.next();
						gLyricistId[k] = resultSet.getInt("currval");
					} else {
						gLyricistId[k] = resultSet.getInt("musicianid");
					}
				}
				//*********************************************

				//追加前にジャンルのidを取得
				sql = "SELECT * FROM genre WHERE genrename=?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, genreName);
				resultSet = pstmt.executeQuery();
				resultSet.next();
				gGenreId = resultSet.getInt("genreid");

				//楽曲を追加しよう
				sql = "INSERT INTO song VALUES(nextval('song_songid_seq'), ?, ?, ?, ?)";
				pstmt = connection.prepareStatement(sql);
				pstmt.setInt(1, gSongNameId);
				pstmt.setInt(2, gProgramNameId);
				pstmt.setInt(3, gGenreId);
				pstmt.setString(4, lyrics);
				pstmt.executeUpdate();

				sql = "SELECT currval('song_songid_seq')";
				pstmt = connection.prepareStatement(sql);
				resultSet = pstmt.executeQuery();
				resultSet.next();
				gSongid = resultSet.getInt("currval");

				//音楽家と楽曲の結び付けを行う
				//歌手*****************************************
				for (int i = 0; i < gSingerId.length; i++) {
					sql = "INSERT INTO assigned_song VALUES(nextval('assigned_song_assigned_song_id_seq'), ?, ?, ?)";
					pstmt = connection.prepareStatement(sql);
					pstmt.setInt(1, gSongid);
					pstmt.setInt(2, 1); //歌手は1
					pstmt.setInt(3, gSingerId[i]);
					pstmt.executeUpdate();
				}
				//*********************************************

				//作曲者***************************************
				for (int j = 0; j < gComposerId.length; j++) {
					sql = "INSERT INTO assigned_song VALUES(nextval('assigned_song_assigned_song_id_seq'), ?, ?, ?)";
					pstmt = connection.prepareStatement(sql);
					pstmt.setInt(1, gSongid);
					pstmt.setInt(2, 2); //作曲者は2
					pstmt.setInt(3, gComposerId[j]);
					pstmt.executeUpdate();
				}
				//*********************************************

				//作詞者***************************************
				for (int k = 0; k < gLyricistId.length; k++) {
					sql = "INSERT INTO assigned_song VALUES(nextval('assigned_song_assigned_song_id_seq'), ?, ?, ?)";
					pstmt = connection.prepareStatement(sql);
					pstmt.setInt(1, gSongid);
					pstmt.setInt(2, 3); //作詞者は3
					pstmt.setInt(3, gLyricistId[k]);
					pstmt.executeUpdate();
				}
				//*********************************************

				resultSet.close();
				connection.close();

			} else { //新規登録以外の処理
				Song oldSong = new Song();

				sql = "SELECT * FROM v_song WHERE songid=?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setInt(1, songID);
				ResultSet resultSet = pstmt.executeQuery();

				if (resultSet.next()) { //普通はあるはず
					oldSong.setSongName(resultSet.getString("songname"));
					oldSong.setProgramName(resultSet.getString("programname"));
					oldSong.setGenreName(resultSet.getString("genrename"));
					oldSong.setLyrics(resultSet.getString("lyrics"));
					oldSong.setSinger(resultSet.getString("singer"));
					oldSong.setComposer(resultSet.getString("composer"));
					oldSong.setLyricist(resultSet.getString("lyricist"));
				} else {
					return;
				}

				//変更前と後での音楽家の数を判定できるようにしておく
				String[] oldSingers = oldSong.getSinger().split(",");
				String[] oldComposers = oldSong.getComposer().split(",");
				String[] oldLyricists = oldSong.getLyricist().split(",");

				//楽曲名変更**********************************************
				sql = "SELECT * FROM songname WHERE songname=?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, songName);

				resultSet = pstmt.executeQuery();
				if (!resultSet.next() && !songName.equals(oldSong.getSongName())) { //変更があって、かつ新しい曲名が見つからないとき追加
					sql = "INSERT INTO songname VALUES(nextval('songname_songnameid_seq'), ?)";
					pstmt = connection.prepareStatement(sql);
					pstmt.setString(1, songName);
					pstmt.executeUpdate();

					sql = "SELECT currval('songname_songnameid_seq')";
					pstmt = connection.prepareStatement(sql);
					resultSet = pstmt.executeQuery();
					resultSet.next();
					gSongNameId = resultSet.getInt("currval");
				} else {
					gSongNameId = resultSet.getInt("songnameid");
				}
				//********************************************************

				//番組名変更**********************************************
				if (programName.equals("")) { //番組名なしはid=0に固定
					gProgramNameId = 0;
				} else {
					sql = "SELECT * FROM programname WHERE programname=?";
					pstmt = connection.prepareStatement(sql);
					pstmt.setString(1, programName);

					resultSet = pstmt.executeQuery();
					if (!resultSet.next() && !oldSong.getProgramName().equals(programName)) { //旧データと一致せず、番組名がなければ追加しよう
						sql = "INSERT INTO programname VALUES(nextval('programname_programnameid_seq'), ?)";
						pstmt = connection.prepareStatement(sql);
						pstmt.setString(1, programName);
						pstmt.executeUpdate();

						sql = "SELECT currval('programname_programnameid_seq')";
						pstmt = connection.prepareStatement(sql);
						resultSet = pstmt.executeQuery();
						resultSet.next();
						gProgramNameId = resultSet.getInt("currval");
					} else {
						gProgramNameId = resultSet.getInt("programnameid");
					}
				}
				//********************************************************

				//歌手名変更**********************************************
				if (oldSingers.length == singer.length) { //同数
					gSingerId = new int[singer.length];
					for (int i = 0; i < singer.length; i++) {
						sql = "SELECT * FROM musician WHERE name=?";
						pstmt = connection.prepareStatement(sql);
						pstmt.setString(1, singer[i]);

						resultSet = pstmt.executeQuery();
						if (!resultSet.next() && !oldSong.getSinger().equals(singer[i])) { //変更があり、歌手名がなければ追加
							sql = "INSERT INTO musician VALUES(nextval('musician_musicianid_seq'), ?)";
							pstmt = connection.prepareStatement(sql);
							pstmt.setString(1, singer[i]);
							pstmt.executeUpdate();

							sql = "SELECT currval('musician_musicianid_seq')";
							pstmt = connection.prepareStatement(sql);
							resultSet = pstmt.executeQuery();
							resultSet.next();
							gSingerId[i] = resultSet.getInt("currval");
						} else {
							gSingerId[i] = resultSet.getInt("musicianid");
						}
					}
				} else if (oldSingers.length < singer.length) {//新しい方が多い
					int i;
					//数が多い方で初期化
					gSingerId = new int[singer.length];
					for (i = 0; i < oldSingers.length; i++) {
						sql = "SELECT * FROM musician WHERE name=?";
						pstmt = connection.prepareStatement(sql);
						pstmt.setString(1, singer[i]);

						resultSet = pstmt.executeQuery();
						if (!resultSet.next() && !oldSong.getSinger().equals(singer[i])) { //変更があり、歌手名がなければ追加
							sql = "INSERT INTO musician VALUES(nextval('musician_musicianid_seq'), ?)";
							pstmt = connection.prepareStatement(sql);
							pstmt.setString(1, singer[i]);
							pstmt.executeUpdate();

							sql = "SELECT currval('musician_musicianid_seq')";
							pstmt = connection.prepareStatement(sql);
							resultSet = pstmt.executeQuery();
							resultSet.next();
							gSingerId[i] = resultSet.getInt("currval");
						} else {
							gSingerId[i] = resultSet.getInt("musicianid");
						}
					}
					//残りを新規登録扱い
					for (; i < singer.length; i++) {
						sql = "SELECT * FROM musician WHERE name=?";
						pstmt = connection.prepareStatement(sql);
						pstmt.setString(1, singer[i]);

						resultSet = pstmt.executeQuery();
						if (!resultSet.next()) { //歌手名がなければ追加
							sql = "INSERT INTO musician VALUES(nextval('musician_musicianid_seq'), ?)";
							pstmt = connection.prepareStatement(sql);
							pstmt.setString(1, singer[i]);
							pstmt.executeUpdate();

							sql = "SELECT currval('musician_musicianid_seq')";
							pstmt = connection.prepareStatement(sql);
							resultSet = pstmt.executeQuery();
							resultSet.next();
							gSingerId[i] = resultSet.getInt("currval");
						} else {
							gSingerId[i] = resultSet.getInt("musicianid");
						}
					}
				} else { //元データの方が多い
					int i;
					gSingerId = new int[singer.length];
					for (i = 0; i < singer.length; i++) {
						sql = "SELECT * FROM musician WHERE name=?";
						pstmt = connection.prepareStatement(sql);
						pstmt.setString(1, singer[i]);

						resultSet = pstmt.executeQuery();
						if (!resultSet.next() && !oldSong.getSinger().equals(singer[i])) { //変更があり、歌手名がなければ追加
							sql = "INSERT INTO musician VALUES(nextval('musician_musicianid_seq'), ?)";
							pstmt = connection.prepareStatement(sql);
							pstmt.setString(1, singer[i]);
							pstmt.executeUpdate();

							sql = "SELECT currval('musician_musicianid_seq')";
							pstmt = connection.prepareStatement(sql);
							resultSet = pstmt.executeQuery();
							resultSet.next();
							gSingerId[i] = resultSet.getInt("currval");
						} else {
							gSingerId[i] = resultSet.getInt("musicianid");
						}
					}
				}
				//********************************************************

				//作曲者変更**********************************************
				if (oldComposers.length == composer.length) { //同数
					gComposerId = new int[composer.length];
					for (int i = 0; i < composer.length; i++) {
						sql = "SELECT * FROM musician WHERE name=?";
						pstmt = connection.prepareStatement(sql);
						pstmt.setString(1, composer[i]);

						resultSet = pstmt.executeQuery();
						if (!resultSet.next() && !oldSong.getSinger().equals(composer[i])) { //変更があり、作曲者がなければ追加
							sql = "INSERT INTO musician VALUES(nextval('musician_musicianid_seq'), ?)";
							pstmt = connection.prepareStatement(sql);
							pstmt.setString(1, composer[i]);
							pstmt.executeUpdate();

							sql = "SELECT currval('musician_musicianid_seq')";
							pstmt = connection.prepareStatement(sql);
							resultSet = pstmt.executeQuery();
							resultSet.next();
							gComposerId[i] = resultSet.getInt("currval");
						} else {
							gComposerId[i] = resultSet.getInt("musicianid");
						}
					}
				} else if (oldComposers.length < composer.length) {//新しい方が多い
					int i;
					//数が多い方で初期化
					gComposerId = new int[composer.length];
					for (i = 0; i < oldComposers.length; i++) {
						sql = "SELECT * FROM musician WHERE name=?";
						pstmt = connection.prepareStatement(sql);
						pstmt.setString(1, composer[i]);

						resultSet = pstmt.executeQuery();
						if (!resultSet.next() && !oldSong.getSinger().equals(composer[i])) { //変更があり、作曲者がなければ追加
							sql = "INSERT INTO musician VALUES(nextval('musician_musicianid_seq'), ?)";
							pstmt = connection.prepareStatement(sql);
							pstmt.setString(1, composer[i]);
							pstmt.executeUpdate();

							sql = "SELECT currval('musician_musicianid_seq')";
							pstmt = connection.prepareStatement(sql);
							resultSet = pstmt.executeQuery();
							resultSet.next();
							gComposerId[i] = resultSet.getInt("currval");
						} else {
							gComposerId[i] = resultSet.getInt("musicianid");
						}
					}
					//残りを新規登録扱い
					for (; i < composer.length; i++) {
						sql = "SELECT * FROM musician WHERE name=?";
						pstmt = connection.prepareStatement(sql);
						pstmt.setString(1, composer[i]);

						resultSet = pstmt.executeQuery();
						if (!resultSet.next()) { //作曲者がなければ追加
							sql = "INSERT INTO musician VALUES(nextval('musician_musicianid_seq'), ?)";
							pstmt = connection.prepareStatement(sql);
							pstmt.setString(1, composer[i]);
							pstmt.executeUpdate();

							sql = "SELECT currval('musician_musicianid_seq')";
							pstmt = connection.prepareStatement(sql);
							resultSet = pstmt.executeQuery();
							resultSet.next();
							gComposerId[i] = resultSet.getInt("currval");
						} else {
							gComposerId[i] = resultSet.getInt("musicianid");
						}
					}
				} else { //元データの方が多い
					int i;
					gComposerId = new int[composer.length];
					for (i = 0; i < composer.length; i++) {
						sql = "SELECT * FROM musician WHERE name=?";
						pstmt = connection.prepareStatement(sql);
						pstmt.setString(1, composer[i]);

						resultSet = pstmt.executeQuery();
						if (!resultSet.next() && !oldSong.getSinger().equals(composer[i])) { //変更があり、作曲者がなければ追加
							sql = "INSERT INTO musician VALUES(nextval('musician_musicianid_seq'), ?)";
							pstmt = connection.prepareStatement(sql);
							pstmt.setString(1, composer[i]);
							pstmt.executeUpdate();

							sql = "SELECT currval('musician_musicianid_seq')";
							pstmt = connection.prepareStatement(sql);
							resultSet = pstmt.executeQuery();
							resultSet.next();
							gComposerId[i] = resultSet.getInt("currval");
						} else {
							gComposerId[i] = resultSet.getInt("musicianid");
						}
					}
				}
				//********************************************************

				//作詞者変更**********************************************
				if (oldLyricists.length == lyricist.length) { //同数
					gLyricistId = new int[lyricist.length];
					for (int i = 0; i < lyricist.length; i++) {
						sql = "SELECT * FROM musician WHERE name=?";
						pstmt = connection.prepareStatement(sql);
						pstmt.setString(1, lyricist[i]);

						resultSet = pstmt.executeQuery();
						if (!resultSet.next() && !oldSong.getSinger().equals(lyricist[i])) { //変更があり、作詞者がなければ追加
							sql = "INSERT INTO musician VALUES(nextval('musician_musicianid_seq'), ?)";
							pstmt = connection.prepareStatement(sql);
							pstmt.setString(1, lyricist[i]);
							pstmt.executeUpdate();

							sql = "SELECT currval('musician_musicianid_seq')";
							pstmt = connection.prepareStatement(sql);
							resultSet = pstmt.executeQuery();
							resultSet.next();
							gLyricistId[i] = resultSet.getInt("currval");
						} else {
							gLyricistId[i] = resultSet.getInt("musicianid");
						}
					}
				} else if (oldLyricists.length < lyricist.length) {//新しい方が多い
					int i;
					//数が多い方で初期化
					gLyricistId = new int[lyricist.length];
					for (i = 0; i < oldLyricists.length; i++) {
						sql = "SELECT * FROM musician WHERE name=?";
						pstmt = connection.prepareStatement(sql);
						pstmt.setString(1, lyricist[i]);

						resultSet = pstmt.executeQuery();
						if (!resultSet.next() && !oldSong.getSinger().equals(lyricist[i])) { //変更があり、作詞者がなければ追加
							sql = "INSERT INTO musician VALUES(nextval('musician_musicianid_seq'), ?)";
							pstmt = connection.prepareStatement(sql);
							pstmt.setString(1, lyricist[i]);
							pstmt.executeUpdate();

							sql = "SELECT currval('musician_musicianid_seq')";
							pstmt = connection.prepareStatement(sql);
							resultSet = pstmt.executeQuery();
							resultSet.next();
							gLyricistId[i] = resultSet.getInt("currval");
						} else {
							gLyricistId[i] = resultSet.getInt("musicianid");
						}
					}
					//残りを新規登録扱い
					for (; i < lyricist.length; i++) {
						sql = "SELECT * FROM musician WHERE name=?";
						pstmt = connection.prepareStatement(sql);
						pstmt.setString(1, lyricist[i]);

						resultSet = pstmt.executeQuery();
						if (!resultSet.next()) { //作詞者がなければ追加
							sql = "INSERT INTO musician VALUES(nextval('musician_musicianid_seq'), ?)";
							pstmt = connection.prepareStatement(sql);
							pstmt.setString(1, lyricist[i]);
							pstmt.executeUpdate();

							sql = "SELECT currval('musician_musicianid_seq')";
							pstmt = connection.prepareStatement(sql);
							resultSet = pstmt.executeQuery();
							resultSet.next();
							gLyricistId[i] = resultSet.getInt("currval");
						} else {
							gLyricistId[i] = resultSet.getInt("musicianid");
						}
					}
				} else { //元データの方が多い
					int i;
					gLyricistId = new int[lyricist.length];
					for (i = 0; i < lyricist.length; i++) {
						sql = "SELECT * FROM musician WHERE name=?";
						pstmt = connection.prepareStatement(sql);
						pstmt.setString(1, lyricist[i]);

						resultSet = pstmt.executeQuery();
						if (!resultSet.next() && !oldSong.getSinger().equals(lyricist[i])) { //変更があり、歌手名がなければ追加
							sql = "INSERT INTO musician VALUES(nextval('musician_musicianid_seq'), ?)";
							pstmt = connection.prepareStatement(sql);
							pstmt.setString(1, lyricist[i]);
							pstmt.executeUpdate();

							sql = "SELECT currval('musician_musicianid_seq')";
							pstmt = connection.prepareStatement(sql);
							resultSet = pstmt.executeQuery();
							resultSet.next();
							gLyricistId[i] = resultSet.getInt("currval");
						} else {
							gLyricistId[i] = resultSet.getInt("musicianid");
						}
					}
				}
				//********************************************************

				//ジャンルは判定なしに変更処理
				sql = "SELECT * FROM genre WHERE genrename=?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, genreName);
				resultSet = pstmt.executeQuery();
				resultSet.next();
				gGenreId = resultSet.getInt("genreid");

				//先に楽曲の更新
				sql = "UPDATE song SET songnameid=?, programnameid=?, genreid=?, lyrics=? WHERE songid=?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setInt(1, gSongNameId);
				pstmt.setInt(2, gProgramNameId);
				pstmt.setInt(3, gGenreId);
				pstmt.setString(4, lyrics);
				pstmt.setInt(5, songID);
				pstmt.executeUpdate();

				//関連付けの変更
				ArrayList<Integer> oldSingerId = new ArrayList<>();
				ArrayList<Integer> oldComposerId = new ArrayList<>();
				ArrayList<Integer> oldLyricistId = new ArrayList<>();

				//歌手*****************************************
				sql = "SELECT * FROM assigned_song WHERE songid=? AND musician_classification_number=?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setInt(1, songID);
				pstmt.setInt(2, 1);//歌手は1
				resultSet = pstmt.executeQuery();

				int i, repeatNum;
				if (oldSingers.length > singer.length) { //少ない場合は再挿入をする形にする
					while (resultSet.next()) {
						oldSingerId.add(resultSet.getInt("assigned_song_id"));
					}
					repeatNum = singer.length;
					//関連付けの削除を行う
					sql = "DELETE FROM assigned_song WHERE songid=? AND musician_classification_number=?";
					pstmt = connection.prepareStatement(sql);
					pstmt.setInt(1, songID);
					pstmt.setInt(2, 1);//歌手は1
					pstmt.executeUpdate();

					//再挿入
					for (i = 0; i < repeatNum; i++) {
						sql = "INSERT INTO assigned_song VALUES(?, ?, ?, ?)";
						pstmt = connection.prepareStatement(sql);
						pstmt.setInt(1, oldSingerId.get(i));
						pstmt.setInt(2, songID);
						pstmt.setInt(3, 1);//歌手は1
						pstmt.setInt(4, gSingerId[i]);
						pstmt.executeUpdate();
					}
				} else {//同数以上はそのまま置き換え
					while (resultSet.next()) {
						oldSingerId.add(resultSet.getInt("musicianid"));
					}
					repeatNum = oldSingerId.size();
					for (i = 0; i < repeatNum; i++) {
						sql = "UPDATE assigned_song SET musicianid=? WHERE songid=? AND musician_classification_number=? AND musicianid=?";
						pstmt = connection.prepareStatement(sql);
						pstmt.setInt(1, gSingerId[i]);
						pstmt.setInt(2, songID);
						pstmt.setInt(3, 1);//歌手は1
						pstmt.setInt(4, oldSingerId.get(i));
						pstmt.executeUpdate();
					}

					if (oldSingerId.size() < singer.length) { //多い場合は新規登録用の関連付けが必要
						for (; i < gSingerId.length; i++) {
							sql = "INSERT INTO assigned_song VALUES(nextval('assigned_song_assigned_song_id_seq'), ?, ?, ?)";
							pstmt = connection.prepareStatement(sql);
							pstmt.setInt(1, songID);
							pstmt.setInt(2, 1); //歌手は1
							pstmt.setInt(3, gSingerId[i]);
							pstmt.executeUpdate();
						}
					}
				}
				//*********************************************

				//作曲者***************************************
				sql = "SELECT * FROM assigned_song WHERE songid=? AND musician_classification_number=?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setInt(1, songID);
				pstmt.setInt(2, 2);//作曲者は2
				resultSet = pstmt.executeQuery();

				int j;
				if (oldComposers.length > composer.length) {//少ない場合は同様に再挿入の形をとる
					while (resultSet.next()) {
						oldComposerId.add(resultSet.getInt("assigned_song_id"));
					}
					repeatNum = composer.length;
					//関連付けの削除を行う
					sql = "DELETE FROM assigned_song WHERE songid=? AND musician_classification_number=?";
					pstmt = connection.prepareStatement(sql);
					pstmt.setInt(1, songID);
					pstmt.setInt(2, 2);//作曲者は2
					pstmt.executeUpdate();

					//再挿入
					for (j = 0; j < repeatNum; j++) {
						sql = "INSERT INTO assigned_song VALUES(?, ?, ?, ?)";
						pstmt = connection.prepareStatement(sql);
						pstmt.setInt(1, oldComposerId.get(j));
						pstmt.setInt(2, songID);
						pstmt.setInt(3, 2);//作曲者は2
						pstmt.setInt(4, gComposerId[j]);
						pstmt.executeUpdate();
					}
				} else {//同数以上はそのまま置き換え
					while (resultSet.next()) {//複数時はwhileに変更
						oldComposerId.add(resultSet.getInt("musicianid"));
					}
					repeatNum = oldComposerId.size();
					for (j = 0; j < repeatNum; j++) {
						sql = "UPDATE assigned_song SET musicianid=? WHERE songid=? AND musician_classification_number=? AND musicianid=?";
						pstmt = connection.prepareStatement(sql);
						pstmt.setInt(1, gComposerId[j]);
						pstmt.setInt(2, songID);
						pstmt.setInt(3, 2);
						pstmt.setInt(4, oldComposerId.get(j));
						pstmt.executeUpdate();
					}

					if (oldComposerId.size() < composer.length) { //多い場合は新規登録用の関連付けが必要
						for (; j < gComposerId.length; j++) {
							sql = "INSERT INTO assigned_song VALUES(nextval('assigned_song_assigned_song_id_seq'), ?, ?, ?)";
							pstmt = connection.prepareStatement(sql);
							pstmt.setInt(1, songID);
							pstmt.setInt(2, 2); //作曲者は2
							pstmt.setInt(3, gComposerId[j]);
							pstmt.executeUpdate();
						}
					}
				}
				//*********************************************

				//作詞者***************************************
				sql = "SELECT * FROM assigned_song WHERE songid=? AND musician_classification_number=?";
				pstmt = connection.prepareStatement(sql);
				pstmt.setInt(1, songID);
				pstmt.setInt(2, 3);//作詞者は3
				resultSet = pstmt.executeQuery();

				int k;
				if (oldLyricists.length > lyricist.length) {//少ない場合は同様に再挿入の形をとる
					while (resultSet.next()) {
						oldLyricistId.add(resultSet.getInt("assigned_song_id"));
					}
					repeatNum = lyricist.length;
					//関連付けの削除を行う
					sql = "DELETE FROM assigned_song WHERE songid=? AND musician_classification_number=?";
					pstmt = connection.prepareStatement(sql);
					pstmt.setInt(1, songID);
					pstmt.setInt(2, 3);//作詞者は3
					pstmt.executeUpdate();
					//再挿入
					for (k = 0; k < repeatNum; k++) {
						sql = "INSERT INTO assigned_song VALUES(?, ?, ?, ?)";
						pstmt = connection.prepareStatement(sql);
						pstmt.setInt(1, oldLyricistId.get(j));
						pstmt.setInt(2, songID);
						pstmt.setInt(3, 3);//作詞者は3
						pstmt.setInt(4, gLyricistId[k]);
						pstmt.executeUpdate();
					}
				} else {//同数以上はそのまま置き換え
					while (resultSet.next()) {
						oldLyricistId.add(resultSet.getInt("musicianid"));
					}
					repeatNum = oldLyricistId.size();
					for (k = 0; k < repeatNum; k++) {
						sql = "UPDATE assigned_song SET musicianid=? WHERE songid=? AND musician_classification_number=? AND musicianid=?";
						pstmt = connection.prepareStatement(sql);
						pstmt.setInt(1, gLyricistId[k]);
						pstmt.setInt(2, songID);
						pstmt.setInt(3, 3);
						pstmt.setInt(4, oldLyricistId.get(k));
						pstmt.executeUpdate();
					}

					if (oldLyricistId.size() < lyricist.length) { //多い場合は新規登録用の関連付けが必要
						for (; k < gLyricistId.length; k++) {
							sql = "INSERT INTO assigned_song VALUES(nextval('assigned_song_assigned_song_id_seq'), ?, ?, ?)";
							pstmt = connection.prepareStatement(sql);
							pstmt.setInt(1, songID);
							pstmt.setInt(2, 3); //作詞者は3
							pstmt.setInt(3, gLyricistId[k]);
							pstmt.executeUpdate();
						}
					}
				}
				//*********************************************

				resultSet.close();
				connection.close();
			}
		} catch (ClassNotFoundException e) {
			logger.error("例外発生", e);
		}
	}

	public void deleteSong(int songID) throws SQLException {
		Connection connection;
		String sql = "DELETE FROM song WHERE songid=?";

		try {
			Class.forName(driverClassName);
			connection = DriverManager.getConnection(url, user, password);

			PreparedStatement pstmt = connection.prepareStatement(sql);

			pstmt = connection.prepareStatement(sql);

			pstmt.setInt(1, songID);
			pstmt.executeUpdate();

			connection.close();
		} catch (ClassNotFoundException e) {
			logger.error("例外発生", e);
			return;
		}
	}

}
