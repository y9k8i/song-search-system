package model;

import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SongManager {

	private Logger logger = LoggerFactory.getLogger(SongManager.class);

	public Song[] searchSong(String songName, String programName, String genreName, String lyrics,
			String singer) {
		ArrayList<Song> resultSong = new ArrayList<>();
		Song[] resultSongArray = null;
		SongDAO getSongData = new SongDAO();
		String gName;

		if (songName == null || songName.equals("")) {
			songName = "";
		}
		if (programName == null || programName.equals("")) {
			programName = "";
		}
		if (genreName == null || genreName.equals("")) {
			genreName = "";
		}
		if (lyrics == null || lyrics.equals("")) {
			lyrics = "";
		}
		if (singer == null || singer.equals("")) {
			singer = "";
		}

		if (genreName.equals("overall")) {
			gName = "総合";
		} else if (genreName.equals("j-pop")) {
			gName = "J-POP";
		} else if (genreName.equals("westan")) {
			gName = "洋楽";
		} else if (genreName.equals("jazz")) {
			gName = "ジャズ";
		} else if (genreName.equals("enka")) {
			gName = "演歌";
		} else if (genreName.equals("k-pop")) {
			gName = "K-POP";
		} else if (genreName.equals("japanese-rock")) {
			gName = "邦楽ロック";
		} else if (genreName.equals("nursery-rhymes")) {
			gName = "童謡";
		} else if (genreName.equals("anime")) {
			gName = "アニメ";
		} else if (genreName.equals("vocaloid")) {
			gName = "ボーカロイド";
		} else if (genreName.equals("game")) {
			gName = "ゲーム";
		} else {
			gName = "";
		}

		try {
			resultSong = getSongData.songReturn(-1, songName, programName, gName, lyrics, singer, singer, singer);
			resultSongArray = (Song[]) resultSong.toArray(new Song[resultSong.size()]);
		} catch (SQLException e) {
			logger.error("例外発生", e);
			return null;
		}
		return resultSongArray;
	}

	/*
	 * 追加要素（楽曲登録の際のチェック）
	 */
	public boolean registSongCheck(String songName, String programName, String genreName, String lyrics, String singer,
			String composer, String lyricist) {

		String checkSongName = songName.trim();
		String checkProgramName = programName.trim();
		String checkSinger = singer.trim();
		String checkLyrics = lyrics.trim();
		String checkComposer = composer.trim();
		String checkLyrisit = lyricist.trim();

		//ここでチェックする（一部は空欄を認めはする）
		if (checkSongName == null || checkSongName.equals("") || checkSongName.equals("　") || checkSongName.contains("\n")) {
			return false;
		}
		if (checkProgramName.equals("　") || checkProgramName.contains(";") || checkProgramName.contains("\n")) {
			return false;
		}
		if (genreName == null || genreName.equals("")) {
			return false;
		}
		if (checkLyrics == null || checkLyrics.equals("") || checkLyrics.equals("　")) {
			return false;
		}
		if (checkSinger == null || checkSinger.equals("") || checkSinger.equals("　") || checkSinger.contains("\n")) {
			return false;
		}
		if (checkComposer.equals("　") || checkComposer.contains("\n")) {
			return false;
		}
		if (checkLyrisit.equals("　") || checkLyrisit.contains("\n")) {
			return false;
		}

		return true;
	}

	public void registerSong(String songName, String programName, String genreName, String lyrics, String singer,
			String composer, String lyricist) {
		SongDAO sDAO = new SongDAO();
		String programNameConv = programName;
		String[] singers;
		String[] composers;
		String[] lyricists;
		String singerConv;
		String composerConv;
		String lyricistConv;
		String lyricsConv;

		//番組名はなくてもいいので、不明の場合の処理
		if (programNameConv == null) {
			programNameConv = "";
		} else {
			programNameConv = programNameConv.trim();
			if (programNameConv.equals("　"))
				programNameConv = "";
		}

		//歌手名等の前後空白削除
		singerConv = singer.trim();
		composerConv = composer.trim();
		lyricistConv = lyricist.trim();

		//作詞者、作曲者はない場合ここで設定
		if (composerConv == null || composerConv.equals("")) {
			composerConv = "不明";
		}
		if (lyricistConv == null || lyricistConv.equals("")) {
			lyricistConv = "不明";
		}

		//複数人の音楽家に対する処理（','と';'のみスプリットする。';'付きのアーティストは','を後につけると登録可能。）
		//全角空白については半角空白にすることで空白入力に対応（一応）
		if (singerConv.contains(",")) {
			singers = singerConv.split(",");
			for (int i = 0; i < singers.length; i++) {
				singers[i] = singers[i].replaceAll("　", " ");
				singers[i] = singers[i].trim();
			}
		} else {
			singers = singerConv.split(";");
			for (int i = 0; i < singers.length; i++) {
				singers[i] = singers[i].replaceAll("　", " ");
				singers[i] = singers[i].trim();
			}
		}
		//作曲家
		if (composerConv.contains(",")) {
			composers = composerConv.split(",");
			for (int i = 0; i < composers.length; i++) {
				composers[i] = composers[i].replaceAll("　", " ");
				composers[i] = composers[i].trim();
			}
		} else {
			composers = composerConv.split(";");
			for (int i = 0; i < composers.length; i++) {
				composers[i] = composers[i].replaceAll("　", " ");
				composers[i] = composers[i].trim();
			}
		}
		//作詞家
		if (lyricistConv.contains(",")) {
			lyricists = lyricistConv.split(",");
			for (int i = 0; i < lyricists.length; i++) {
				lyricists[i] = lyricists[i].replaceAll("　", " ");
				lyricists[i] = lyricists[i].trim();
			}
		} else {
			lyricists = lyricistConv.split(";");
			for (int i = 0; i < lyricists.length; i++) {
				lyricists[i] = lyricists[i].replaceAll("　", " ");
				lyricists[i] = lyricists[i].trim();
			}
		}

		//歌詞の改行コードの修正処理
		lyricsConv = lyrics.replaceAll("\r\n", "\n");

		try {
			sDAO.updateSong(-1, songName, programNameConv, genreName, lyricsConv, singers, composers,
					lyricists, true);
		} catch (SQLException e) {
			logger.error("例外発生", e);
		}
	}

	public void changeSong(String songID, String songName, String programName, String genreName, String lyrics,
			String singer, String composer, String lyricist) {
		SongDAO sDao = new SongDAO();
		String programNameConv = programName;
		String composerConv;
		String singerConv;
		String lyricistConv;
		String lyricsConv;
		String[] singers;
		String[] composers;
		String[] lyricists;

		//番組名はなくてもいいので、不明の場合の処理
		if (programNameConv == null) {
			programNameConv = "";
		} else {
			programNameConv = programNameConv.trim();
			if (programNameConv.equals("　"))
				programNameConv = "";
		}

		//歌手名等の前後空白削除
		singerConv = singer.trim();
		composerConv = composer.trim();
		lyricistConv = lyricist.trim();

		//作詞者、作曲者はない場合ここで設定
		if (composerConv == null || composerConv.equals("")) {
			composerConv = "不明";
		}
		if (lyricistConv == null || lyricistConv.equals("")) {
			lyricistConv = "不明";
		}

		//歌詞の改行コードの修正処理
		lyricsConv = lyrics.replaceAll("\r\n", "\n");

		//複数人の音楽家に対する処理（','と';'のみスプリットする。';'付きのアーティストは','を後につけると登録可能。）
		//全角空白については半角空白にすることで空白入力に対応（一応）
		if (singerConv.contains(",")) {
			singers = singerConv.split(",");
			for (int i = 0; i < singers.length; i++) {
				singers[i] = singers[i].replaceAll("　", " ");
				singers[i] = singers[i].trim();
			}
		} else {
			singers = singerConv.split(";");
			for (int i = 0; i < singers.length; i++) {
				singers[i] = singers[i].replaceAll("　", " ");
				singers[i] = singers[i].trim();
			}
		}
		//作曲家
		if (composerConv.contains(",")) {
			composers = composerConv.split(",");
			for (int i = 0; i < composers.length; i++) {
				composers[i] = composers[i].replaceAll("　", " ");
				composers[i] = composers[i].trim();
			}
		} else {
			composers = composerConv.split(";");
			for (int i = 0; i < composers.length; i++) {
				composers[i] = composers[i].replaceAll("　", " ");
				composers[i] = composers[i].trim();
			}
		}
		//作詞家
		if (lyricistConv.contains(",")) {
			lyricists = lyricistConv.split(",");
			for (int i = 0; i < lyricists.length; i++) {
				lyricists[i] = lyricists[i].replaceAll("　", " ");
				lyricists[i] = lyricists[i].trim();
			}
		} else {
			lyricists = lyricistConv.split(";");
			for (int i = 0; i < lyricists.length; i++) {
				lyricists[i] = lyricists[i].replaceAll("　", " ");
				lyricists[i] = lyricists[i].trim();
			}
		}

		try {
			sDao.updateSong(Integer.parseInt(songID), songName, programNameConv, genreName, lyricsConv, singers,
					composers, lyricists, false);
		} catch (SQLException e) {
			logger.error("例外発生", e);
		}
	}

	public void deleteSong(String songID) {
		SongDAO sDao = new SongDAO();

		try {
			sDao.deleteSong(Integer.parseInt(songID));
		} catch (SQLException e) {
			logger.error("例外発生", e);
		}
	}
}
