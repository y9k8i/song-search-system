package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import model.Song;
import model.SongManager;

/**
 * Servlet implementation class SerarchController
 */
@WebServlet("/SearchController")
public class SearchController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private SongManager s = new SongManager();
	Logger logger = LoggerFactory.getLogger(SearchController.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SearchController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//GETリクエストは許可していない
		response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
			if (request.getParameter("action").equals("search")) {//検索処理
				if ((boolean) session.getAttribute("login")) {
					String songName = request.getParameter("songName");
					String singer = request.getParameter("singer");
					String programName = request.getParameter("programName");
					String genreName = request.getParameter("genreName");
					String lyrics = request.getParameter("lyrics");

					if (songName != null && !songName.equals("")) {
						session.setAttribute("searchWord", songName);
					} else if (singer != null && !singer.equals("")) {
						session.setAttribute("searchWord", singer);
					} else if (programName != null && !programName.equals("")) {
						session.setAttribute("searchWord", programName);
					} else if (genreName != null && !genreName.equals("")) {
						session.setAttribute("searchWord", "ジャンルで");
					} else if (lyrics != null && !lyrics.equals("")) {
						session.setAttribute("searchWord", "歌詞のみ");
					} else {
						session.setAttribute("cautionS", true);
						getServletContext().getRequestDispatcher("/top.jsp").forward(request, response);
						return;
					}

					Song[] arraySongs = s.searchSong(songName, programName, genreName, lyrics, singer);

					if (arraySongs != null && arraySongs.length > 0) {
						session.setAttribute("search", true);
						session.setAttribute("result", arraySongs);
					} else {
						session.setAttribute("search", false);
					}
					session.setAttribute("cautionS", false);
					getServletContext().getRequestDispatcher("/resultSong.jsp").forward(request, response);
				} else {
					response.setContentType("text/html; charset=UTF-8");
					PrintWriter out = response.getWriter();
					out.println("<p>ログアウト状態ではできません。もう一度やり直してください</p>");
					out.println("<a href=\"./index.jsp\">ログインページ</a>");
					session.invalidate();
				}
			} else if (request.getParameter("action").equals("registerSong")) {//楽曲登録画面への遷移処理
				if (session.getAttribute("admin") != null && (boolean) session.getAttribute("admin")) {
					session.setAttribute("newTitle", null);
					session.setAttribute("newProgram", null);
					session.setAttribute("newSinger", null);
					session.setAttribute("newLyrics", null);
					session.setAttribute("newComposer", null);
					session.setAttribute("newLyricist", null);
					session.setAttribute("caution", false);

					getServletContext().getRequestDispatcher("/registerSong.jsp").forward(request, response);
				} else {
					response.setContentType("text/html; charset=UTF-8");
					PrintWriter out = response.getWriter();
					out.append("<p>管理者専用です</p>");
					out.append("<a href=\"./index.jsp\">ログインページ</a>");
					session.invalidate();
					return;
				}
			} else if (request.getParameter("action").equals("checkSend")) { //楽曲登録確認画面への遷移処理
				String songName = request.getParameter("newTitle");
				String singer = request.getParameter("newSinger");
				String programName = request.getParameter("newProgram");
				String genreName = request.getParameter("newGenre");
				String lyrics = request.getParameter("newLyrics");
				String composer = request.getParameter("newComposer");
				String lyricist = request.getParameter("newLyricist");
				if (s.registSongCheck(songName, programName, genreName, lyrics, singer, composer, lyricist)) {
					Song checked = new Song();
					checked.setSongName(songName);
					checked.setSinger(singer);
					checked.setProgramName(programName);
					//ジャンルの日本語変換
					if (genreName.equals("overall")) {
						genreName = "総合";
					} else if (genreName.equals("j-pop")) {
						genreName = "J-POP";
					} else if (genreName.equals("westan")) {
						genreName = "洋楽";
					} else if (genreName.equals("jazz")) {
						genreName = "ジャズ";
					} else if (genreName.equals("enka")) {
						genreName = "演歌";
					} else if (genreName.equals("k-pop")) {
						genreName = "K-POP";
					} else if (genreName.equals("japanese-rock")) {
						genreName = "邦楽ロック";
					} else if (genreName.equals("nursery-rhymes")) {
						genreName = "童謡";
					} else if (genreName.equals("anime")) {
						genreName = "アニメ";
					} else if (genreName.equals("vocaloid")) {
						genreName = "ボーカロイド";
					} else if (genreName.equals("game")) {
						genreName = "ゲーム";
					} else {
						genreName = "";
					}
					checked.setGenreName(genreName);
					checked.setLyrics(lyrics);
					checked.setComposer(composer);
					checked.setLyricist(lyricist);
					session.setAttribute("caution", false);
					session.setAttribute("registSong", checked);
					getServletContext().getRequestDispatcher("/confirmRegisterSong.jsp").forward(request, response);
				} else {
					//入力時うざかったので修正
					session.setAttribute("newTitle", songName);
					session.setAttribute("newProgram", programName);
					session.setAttribute("newSinger", singer);
					session.setAttribute("newLyrics", lyrics);
					session.setAttribute("newComposer", composer);
					session.setAttribute("newLyricist", lyricist);
					session.setAttribute("caution", true);
					getServletContext().getRequestDispatcher("/registerSong.jsp").forward(request, response);
				}
			} else if (request.getParameter("action").equals("regist")) { //楽曲登録処理と後処理
				if (session.getAttribute("admin") != null && (boolean) session.getAttribute("admin")) {
					Song registSong = (Song) session.getAttribute("registSong");
					s.registerSong(registSong.getSongName(), registSong.getProgramName(), registSong.getGenreName(),
							registSong.getLyrics(), registSong.getSinger(), registSong.getComposer(), registSong.getLyricist());
					session.setAttribute("registSong", null);
					getServletContext().getRequestDispatcher("/registerCompleteSong.jsp").forward(request, response);
				} else {
					response.setContentType("text/html; charset=UTF-8");
					PrintWriter out = response.getWriter();
					out.append("<p>管理者専用です</p>");
					out.append("<a href=\"./index.jsp\">ログインページ</a>");
					session.invalidate();
					return;
				}
			} else if (request.getParameter("action").equals("changeSong")) { //楽曲情報変更画面へ行く処理
				Song tmpSong = (Song) session.getAttribute("detailSong");
				session.setAttribute("changeSong", null);
				session.setAttribute("oldTitle", tmpSong.getSongName());
				session.setAttribute("oldSinger", tmpSong.getSinger());
				session.setAttribute("oldProgram", tmpSong.getProgramName());
				session.setAttribute("oldGenre", tmpSong.getGenreName());
				session.setAttribute("oldLyrics", tmpSong.getLyrics());
				session.setAttribute("oldComposer", tmpSong.getComposer());
				session.setAttribute("oldLyricist", tmpSong.getLyricist());
				session.setAttribute("caution", false);

				getServletContext().getRequestDispatcher("/changeSong.jsp").forward(request, response);
			} else if (request.getParameter("action").equals("checkSend2")) { //楽曲変更確認画面へ行く処理
				boolean checkSong = s.registSongCheck(request.getParameter("newTitle"),
						request.getParameter("newProgram"),
						request.getParameter("newGenre"), request.getParameter("newLyrics"),
						request.getParameter("newSinger"), "", "");
				if (checkSong) {
					Song tmpSong = (Song) session.getAttribute("detailSong");
					String genreName = request.getParameter("newGenre");

					tmpSong.setSongName(request.getParameter("newTitle"));
					tmpSong.setProgramName(request.getParameter("newProgram"));
					//ジャンルの日本語変換
					if (genreName.equals("overall")) {
						genreName = "総合";
					} else if (genreName.equals("j-pop")) {
						genreName = "J-POP";
					} else if (genreName.equals("westan")) {
						genreName = "洋楽";
					} else if (genreName.equals("jazz")) {
						genreName = "ジャズ";
					} else if (genreName.equals("enka")) {
						genreName = "演歌";
					} else if (genreName.equals("k-pop")) {
						genreName = "K-POP";
					} else if (genreName.equals("japanese-rock")) {
						genreName = "邦楽ロック";
					} else if (genreName.equals("nursery-rhymes")) {
						genreName = "童謡";
					} else if (genreName.equals("anime")) {
						genreName = "アニメ";
					} else if (genreName.equals("vocaloid")) {
						genreName = "ボーカロイド";
					} else if (genreName.equals("game")) {
						genreName = "ゲーム";
					} else {
						genreName = "";
					}
					tmpSong.setGenreName(genreName);
					tmpSong.setSinger(request.getParameter("newSinger"));
					tmpSong.setLyrics(request.getParameter("newLyrics"));
					tmpSong.setComposer(request.getParameter("newComposer"));
					tmpSong.setLyricist(request.getParameter("newLyricist"));

					session.setAttribute("changeSong", tmpSong);
					session.setAttribute("caution", false);
					getServletContext().getRequestDispatcher("/changeConfirmSong.jsp").forward(request, response);
				} else {
					session.setAttribute("caution", true);
					getServletContext().getRequestDispatcher("/changeSong.jsp").forward(request, response);
				}
			} else if (request.getParameter("action").equals("change")) { //楽曲変更と変更後の後処理
				if (session.getAttribute("admin") != null && (boolean) session.getAttribute("admin")) {
					Song changeSong = (Song) session.getAttribute("changeSong");
					s.changeSong(changeSong.getSongID(), changeSong.getSongName(), changeSong.getProgramName(),
							changeSong.getGenreName(), changeSong.getLyrics(), changeSong.getSinger(),
							changeSong.getComposer(), changeSong.getLyricist());

					session.setAttribute("oldTitle", null);
					session.setAttribute("oldSinger", null);
					session.setAttribute("oldProgram", null);
					session.setAttribute("oldGenre", null);
					session.setAttribute("oldLyrics", null);
					session.setAttribute("oldComposer", null);
					session.setAttribute("oldLyricist", null);
					session.setAttribute("changeSong", null);
					getServletContext().getRequestDispatcher("/changeCompleteSong.jsp").forward(request, response);
				} else {
					response.setContentType("text/html; charset=UTF-8");
					PrintWriter out = response.getWriter();
					out.append("<p>管理者専用です</p>");
					out.append("<a href=\"./index.jsp\">ログインページ</a>");
					session.invalidate();
					return;
				}
			} else if (request.getParameter("action").equals("deleteSong")) { //楽曲削除確認画面へ行く処理
				if (session.getAttribute("admin") != null && (boolean) session.getAttribute("admin")) {
					Song tSong = (Song) session.getAttribute("detailSong");
					session.setAttribute("detailDel", tSong);

					getServletContext().getRequestDispatcher("/deleteSong.jsp").forward(request, response);
				} else {
					response.setContentType("text/html; charset=UTF-8");
					PrintWriter out = response.getWriter();
					out.append("<p>管理者専用です</p>");
					out.append("<a href=\"./index.jsp\">ログインページ</a>");
					session.invalidate();
					return;
				}
			} else if (request.getParameter("action").equals("delete")) { //楽曲の削除とあと処理
				if (session.getAttribute("admin") != null && (boolean) session.getAttribute("admin")) {
					Song dSong = (Song) session.getAttribute("detailDel");
					s.deleteSong(dSong.getSongID());

					session.setAttribute("detailSong", null);
					session.setAttribute("detailDel", null);
					getServletContext().getRequestDispatcher("/deleteCompleteSong.jsp").forward(request, response);
				} else {
					response.setContentType("text/html; charset=UTF-8");
					PrintWriter out = response.getWriter();
					out.append("<p>管理者専用です</p>");
					out.append("<a href=\"./index.jsp\">ログインページ</a>");
					session.invalidate();
					return;
				}
			} else {
				/* ここへ来た場合は無条件でエラー表示 */
				response.setContentType("text/html; charset=UTF-8");
				PrintWriter out = response.getWriter();
				out.append("<p>もう一度やり直してください</p>");
				out.append("<a href=\"./index.jsp\">ログインページ</a>");
				return;
			}
		} catch (NullPointerException e) {
			logger.error("NullPointerException Error", e);
			PrintWriter out = response.getWriter();
			/* セッションが開始されずにここへ来た場合は無条件でエラー表示 */
			out.println("<!DOCTYPE html><html lang=\"ja\">");
			out.println("<head><meta http-equiv=\"Refresh\" content=\"5;URL=index.jsp\"></head>");
			out.println("<body><h1>Bad Session</h1></body>");
			out.println("</html>");
			return;
		}
	}

}
