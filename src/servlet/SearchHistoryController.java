package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import model.SearchHistory;
import model.SearchHistoryManager;
import model.Song;

/**
 * Servlet implementation class SearchHistoryController
 */
@WebServlet("/SearchHistoryController")
public class SearchHistoryController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private SearchHistoryManager shm = new SearchHistoryManager();
	Logger logger = LoggerFactory.getLogger(SearchHistoryController.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SearchHistoryController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//GETリクエストは許可していない
		response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
			if (request.getParameter("songid") != null) { //楽曲詳細表示（履歴に登録とともに、表示の処理を担う）
				shm.registerHistory((String)session.getAttribute("id"), request.getParameter("songid"));

				session.setAttribute("detailSong", shm.getSong());
				getServletContext().getRequestDispatcher("/informationSong.jsp").forward(request, response);
			} else if(request.getParameter("action").equals("history")) { //履歴表示処理
				SearchHistory[] arrayHist = shm.searchHistory((String)session.getAttribute("id"));
				Song[] arraySong = shm.getSongs();

				if(arrayHist.length > 0) {
					session.setAttribute("history", true);
					session.setAttribute("histSongInfo", arraySong);
					session.setAttribute("histData", arrayHist);
				}else {
					session.setAttribute("history", false);
				}
				getServletContext().getRequestDispatcher("/historySong.jsp").forward(request, response);
			} else {
				/* ここへ来た場合は無条件でエラー表示 */
				response.setContentType("text/html; charset=UTF-8");
				PrintWriter out = response.getWriter();
				out.append("<p>もう一度やり直してください</p>");
				out.append("<a href=\"./index.jsp\">ログインページ</a>");
				return;
			}
		} catch (NullPointerException e) {
			logger.error("NullPointerException Error", e);
			PrintWriter out = response.getWriter();
			/* セッションが開始されずにここへ来た場合は無条件でエラー表示 */
			out.println("<!DOCTYPE html><html lang=\"ja\">");
			out.println("<head><meta http-equiv=\"Refresh\" content=\"5;URL=index.jsp\"></head>");
			out.println("<body><h1>Bad Session</h1></body>");
			out.println("</html>");
			return;
		}
	}

}
