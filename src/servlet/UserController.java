package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import model.Member;
import model.MemberManager;

/**
 * Servlet implementation class Controller
 */
@WebServlet("/UserController")
public class UserController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private MemberManager m = new MemberManager();
	Logger logger = LoggerFactory.getLogger(UserController.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//GETリクエストは許可していない
		response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
			if (request.getParameter("action") != null) {
				if (request.getParameter("action").equals("login")) { //ログイン処理
					boolean checkLogin = false;
					checkLogin = m.login(request.getParameter("id"), request.getParameter("password"), "newCheck");
					if (checkLogin) {
						session.setAttribute("login", true);
						if (request.getParameter("id").length() == 4) {
							session.setAttribute("admin", true);
						}
						session.setAttribute("id", m.getMember().getUserID());
						session.setAttribute("pass", m.getMember().getPassword());
						logger.info("Login OK {}", request.getParameter("id"));
						getServletContext().getRequestDispatcher("/top.jsp").forward(request, response);
					} else {
						session.setAttribute("login", false);
						getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
					}

				} else if (request.getParameter("action").equals("checkSend")) { //登録から確認画面へ行くための処理
					//ログインしている状態では判定させない
					if (session.getAttribute("login") == null
							|| (session.getAttribute("login") != null && !(boolean) session.getAttribute("login"))) {
						int newAge = -1;
						Member tmpMem = new Member();
						//年齢は数値であることを確認（変換する過程で）
						try {
							newAge = Integer.parseInt(request.getParameter("newAge"));
						} catch (NumberFormatException e) {
							session.setAttribute("caution", true);
							getServletContext().getRequestDispatcher("/registerMember.jsp").forward(request, response);
							return;
						}
						//エラー処理（それ以外の入力処理を投げて判定させる）
						if (!m.checkMember(request.getParameter("newId"), request.getParameter("newName"),
								request.getParameter("newPassword"), newAge, request.getParameter("newSex"))) {
							session.setAttribute("caution", true);
							getServletContext().getRequestDispatcher("/registerMember.jsp").forward(request, response);
							return;
						} else {
							tmpMem = m.getMember();
							session.setAttribute("newId", request.getParameter("newId"));
							session.setAttribute("newName", request.getParameter("newName"));
							session.setAttribute("newAge", request.getParameter("newAge"));
							session.setAttribute("newSex", tmpMem.getSex());
						}
						session.setAttribute("caution", false);
						getServletContext().getRequestDispatcher("/registerConfirmMember.jsp").forward(request,
								response);
					} else {
						response.setContentType("text/html; charset=UTF-8");
						PrintWriter out = response.getWriter();
						out.append("<p>ログイン状態ではできません。もう一度やり直してください</p>");
						out.append("<a href=\"./index.jsp\">ログインページ</a>");
						session.invalidate();
					}
				} else if (request.getParameter("action").equals("regist")) { //登録と登録後の処理
					//ログイン状態では登録させない
					if (session.getAttribute("login") == null
							|| (session.getAttribute("login") != null && !(boolean) session.getAttribute("login"))) {
						m.registerMember(m.getMember().getUserID(), m.getMember().getUserName(),
								m.getMember().getPassword(), m.getMember().getAge(), m.getMember().getSex());

						session.invalidate();
						getServletContext().getRequestDispatcher("/registerCompleteMember.jsp").forward(request,
								response);
					} else {
						response.setContentType("text/html; charset=UTF-8");
						PrintWriter out = response.getWriter();
						out.println("<p>ログイン状態ではできません。もう一度やり直してください</p>");
						out.println("<a href=\"./index.jsp\">ログインページ</a>");
						session.invalidate();
					}

				} else if (request.getParameter("action").equals("changeMemberInfo")) {//会員情報変更画面へ行く処理
					boolean checkLogin = false;
					checkLogin = m.login((String) session.getAttribute("id"), (String) session.getAttribute("pass"),
							"check");
					if (checkLogin) {
						session.setAttribute("oldId", m.getMember().getUserID());
						session.setAttribute("oldUserName", m.getMember().getUserName());
						session.setAttribute("oldAge", m.getMember().getAge());
						session.setAttribute("oldSex", m.getMember().getSex());
						session.setAttribute("caution", false);
						getServletContext().getRequestDispatcher("/changeMember.jsp").forward(request, response);
					} else {
						response.setContentType("text/html; charset=UTF-8");
						PrintWriter out = response.getWriter();
						/* ここへ来た場合は無条件でエラー表示 */
						out.println("<p>もう一度やり直してください</p>");
						out.println("<a href=\"./index.jsp\">ログインページ</a>");
						return;
					}
				} else if (request.getParameter("action").equals("changeMemberInfoAd")) {//会員情報変更画面へ行く処理（管理者用）
					boolean checkLogin2 = false;
					checkLogin2 = m.login((String) session.getAttribute("id"), (String) session.getAttribute("pass"),
							"check");
					if (checkLogin2 && (boolean) session.getAttribute("admin")) {
						Member tmpMem = (Member) session.getAttribute("detailMember");
						session.setAttribute("oldId", tmpMem.getUserID());
						session.setAttribute("oldUserName", tmpMem.getUserName());
						session.setAttribute("oldAge", tmpMem.getAge());
						session.setAttribute("oldSex", tmpMem.getSex());
						session.setAttribute("caution", false);
						getServletContext().getRequestDispatcher("/changeMember.jsp").forward(request, response);
					} else {
						response.setContentType("text/html; charset=UTF-8");
						PrintWriter out = response.getWriter();
						/* ここへ来た場合は無条件でエラー表示 */
						out.println("<p>もう一度やり直してください</p>");
						out.println("<a href=\"./index.jsp\">ログインページ</a>");
						return;
					}
				} else if (request.getParameter("action").equals("checkSend2")) { //変更画面から確認画面へ行くための処理
					int newAge = -1;
					//年齢は数値であることを確認（変換する過程で）
					try {
						newAge = Integer.parseInt(request.getParameter("newAge"));
					} catch (NumberFormatException e) {
						session.setAttribute("caution", true);
						getServletContext().getRequestDispatcher("/changeMember.jsp").forward(request, response);
						return;
					}
					//エラー処理（それ以外の入力処理を投げて判定させる）
					if (!m.checkMember(request.getParameter("newId"), request.getParameter("newName"),
							request.getParameter("newPassword"), newAge, request.getParameter("newSex"))) {
						session.setAttribute("caution", true);
						getServletContext().getRequestDispatcher("/changeMember.jsp").forward(request, response);
						return;
					} else {
						session.setAttribute("newId", request.getParameter("newId"));
						session.setAttribute("newName", request.getParameter("newName"));
						session.setAttribute("newAge", request.getParameter("newAge"));
						session.setAttribute("newSex", m.getMember().getSex());
					}
					session.setAttribute("caution", false);
					getServletContext().getRequestDispatcher("/confirmChangeMember.jsp").forward(request, response);

				} else if (request.getParameter("action").equals("change")) { //変更と変更後の処理
					if (session.getAttribute("admin") != null && (boolean) session.getAttribute("admin")) { //管理者による変更用
						Member tmpMem = (Member) session.getAttribute("detailMember");

						m.changeMember(m.getMember().getUserID(), m.getMember().getUserName(),m.getMember().getPassword(),
								m.getMember().getAge(), m.getMember().getSex(), tmpMem);
						logger.info("Change Member Info {} by {}", m.getMember().getUserID(), "admin");
					} else if(session.getAttribute("admin") == null || !(boolean) session.getAttribute("admin")){ //一般用
						MemberManager oldData = new MemberManager();
						//旧データを持ってくる
						oldData.login((String) session.getAttribute("id"), (String) session.getAttribute("pass"),
								"check");
						//処理開始
						m.changeMember(m.getMember().getUserID(), m.getMember().getUserName(),
								m.getMember().getPassword(),m.getMember().getAge(), m.getMember().getSex(), oldData.getMember());
						session.setAttribute("id", m.getMember().getUserID());
						session.setAttribute("pass", m.getMember().getPassword());
						logger.info("Change Member Info {} to {}", oldData.getMember().getUserID(), (String) session.getAttribute("id"));
					}
					session.setAttribute("newId", null);
					session.setAttribute("newName", null);
					session.setAttribute("newAge", null);
					session.setAttribute("newSex", null);
					session.setAttribute("oldId", null);
					session.setAttribute("oldUserName", null);
					session.setAttribute("oldAge", null);
					session.setAttribute("oldSex", null);
					session.setAttribute("detailMember", null);
					getServletContext().getRequestDispatcher("/changeCompleteMember.jsp").forward(request, response);
				} else if (request.getParameter("action").equals("listMember")) { //会員一覧表示処理
					boolean checkLogin = false;
					checkLogin = m.login((String) session.getAttribute("id"), (String) session.getAttribute("pass"),
							"check");
					if (checkLogin && session.getAttribute("admin") != null && (boolean) session.getAttribute("admin")) {
						Member[] memberArray = m.returnMembers((String) session.getAttribute("id"),
								(String) session.getAttribute("pass"));
						session.setAttribute("allMem", memberArray);
						getServletContext().getRequestDispatcher("/tableMember.jsp").forward(request, response);
					} else {
						response.setContentType("text/html; charset=UTF-8");
						PrintWriter out = response.getWriter();
						/* ここへ来た場合は無条件でエラー表示 */
						out.append("<p>もう一度やり直してください</p>");
						out.append("<a href=\"./index.jsp\">ログインページ</a>");
						return;
					}
				} else if (request.getParameter("action").equals("deleteMember")) {//削除前の確認画面へ移行
					Member tmpMem = (Member) session.getAttribute("detailMember");
					session.setAttribute("delMem", tmpMem.getUserID());
					getServletContext().getRequestDispatcher("/deleteMember.jsp").forward(request, response);
				} else if (request.getParameter("action").equals("delete")) {//会員の削除と後処理
					m.deleteMember((String) session.getAttribute("delMem"));
					session.setAttribute("detailMember", null);
					session.setAttribute("delMem", null);
					session.setAttribute("oldId", null);
					session.setAttribute("oldUserName", null);
					session.setAttribute("oldAge", null);
					session.setAttribute("oldSex", null);
					getServletContext().getRequestDispatcher("/deleteCompleteMember.jsp").forward(request, response);
				} else if (request.getParameter("action").equals("logout")) { //ログアウト処理
					logger.info("Logout OK {}", (String)session.getAttribute("id"));
					session.invalidate();
					getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
				} else {
					response.setContentType("text/html; charset=UTF-8");
					PrintWriter out = response.getWriter();
					/* ここへ来た場合は無条件でエラー表示 */
					out.append("<p>もう一度やり直してください</p>");
					out.append("<a href=\"./index.jsp\">ログインページ</a>");
					return;
				}
			} else if (request.getParameter("userid") != null) {//詳細情報表示ページ用処理
				Member member = new Member();
				if (session.getAttribute("admin") != null && (boolean) session.getAttribute("admin")) {
					member = m.returnMember((String) session.getAttribute("id"),
							(String) session.getAttribute("pass"), request.getParameter("userid"));
					session.setAttribute("detailMember", member);
					getServletContext().getRequestDispatcher("/informationMember.jsp").forward(request, response);
				} else {
					response.setContentType("text/html; charset=UTF-8");
					PrintWriter out = response.getWriter();
					/* ここへ来た場合は無条件でエラー表示 */
					out.append("<p>もう一度やり直してください</p>");
					out.append("<a href=\"./index.jsp\">ログインページ</a>");
					return;
				}
			} else {
				response.setContentType("text/html; charset=UTF-8");
				PrintWriter out = response.getWriter();
				/* ここへ来た場合は無条件でエラー表示 */
				out.append("<p>もう一度やり直してください</p>");
				out.append("<a href=\"./index.jsp\">ログインページ</a>");
				return;
			}
		} catch (NullPointerException e) {
			logger.error("NullPointerException Error", e);
			PrintWriter out = response.getWriter();
			/* セッションが開始されずにここへ来た場合は無条件でエラー表示 */
			out.println("<!DOCTYPE html><html lang=\"ja\">");
			out.println("<head><meta http-equiv=\"Refresh\" content=\"5;URL=index.jsp\"></head>");
			out.println("<body><h1>Bad Session</h1></body>");
			out.println("</html>");
			return;
		}
	}

}
