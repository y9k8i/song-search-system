package model;

import org.junit.Test;
import static org.junit.Assert.*;

public class MemberTest {
	Member member = new Member();
    @Test
    public void testString() {
		member.setUserName("名前");
		member.setSex("男");
        assertEquals("名前", member.getUserName());
        assertEquals("男", member.getSex());
	}
    @Test
    public void testInt() {
		member.setAge(20);
        assertEquals(20, member.getAge());
	}
}
