package model;

import static org.junit.Assert.*;

import org.junit.Test;

public class SearchHistoryTest {
	SearchHistory history = new SearchHistory();
    @Test
    public void testString() {
    	history.setUserID("ユーザID");
    	history.setSongID("楽曲ID");
        assertEquals("ユーザID", history.getUserID());
        assertEquals("楽曲ID", history.getSongID());
	}
    @Test
    public void testInt() {
    	history.setSearchDate(12345);
        assertEquals(12345, history.getSearchDate());
	}
}
