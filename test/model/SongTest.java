package model;

import static org.junit.Assert.*;

import org.junit.Test;

public class SongTest {
	Song song = new Song();
	@Test
    public void testString() {
		song.setSongID("楽曲ID");
		song.setSongName("楽曲名");
		song.setProgramName("番組名");
		song.setGenreName("ジャンル");
		song.setLyrics("歌詞");
		song.setSinger("歌手");
		song.setComposer("作曲者");
		song.setLyricist("作詞者");
		assertEquals("楽曲ID", song.getSongID());
		assertEquals("楽曲名", song.getSongName());
		assertEquals("番組名", song.getProgramName());
		assertEquals("ジャンル", song.getGenreName());
		assertEquals("歌詞", song.getLyrics());
		assertEquals("歌手", song.getSinger());
		assertEquals("作曲者", song.getComposer());
		assertEquals("作詞者", song.getLyricist());
	}
}
